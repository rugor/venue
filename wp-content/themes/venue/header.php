<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<title><?php bloginfo('name'); ?> | <?php bloginfo('description'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php bloginfo('url'); ?>/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php bloginfo('url'); ?>/favicon.ico" type="image/x-icon">
	<script type="text/javascript">
		var HOMEURL = "<?php bloginfo('url'); ?>";
		var THEMEURL = "<?php bloginfo("template_directory"); ?>";
	</script>
	<script type="text/javascript">
		  WebFontConfig = {
		    google: { families: [ 'Open+Sans:400,600,400italic,600italic,700,700italic:latin' ] }
		  };
		  (function() {
		    var wf = document.createElement('script');
		    wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
		    wf.type = 'text/javascript';
		    wf.async = 'true';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(wf, s);
		  })();
	</script>
	<?php 
		wp_head();
		$logo_uploader = of_get_option('logo_uploader', 'no entry' );
		$logo_small_uploader = of_get_option('logo_small_uploader', 'no entry' );
		$disclaimer_text = of_get_option('disclaimer_text', 'no entry' );
	?>
</head>

<body <?php body_class(); ?>>
	<div id="header">
		<div id="feature_image">
			<?php if ( $logo_small_uploader ) { ?>
				<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.min.svg" id="logo" alt="Venue"/></a>
			<?php } ?>
		</div><!-- feature image -->
		<div id="navigation">
				<ul id="corpmenu">
					<li class="menu-item <?php if (is_page('venue')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/venue"><span>Venue</span></a></li>
					<li class="menu-item <?php if (is_page('history')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/history"><span>History</span></a>
						<ul class="sub-menu" style="display: none;">
							<li><a href="<?php bloginfo('url'); ?>/history/past"><span>Past</span></a></li>
							<li><a href="<?php bloginfo('url'); ?>/history/present"><span>Present</span></a></li>
							<li><a href="<?php bloginfo('url'); ?>/history/future"><span>Future</span></a></li>
						</ul>
					</li>
					<li class="menu-item <?php if (is_page('neighbourhood')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/neighbourhood"><span>Neighbourhood</span></a>
						<ul class="sub-menu" style="display: none;">
							<li><a href="<?php bloginfo('url'); ?>/neighbourhood/quattro"><span>Quattro</span></a></li>
							<li><a href="<?php bloginfo('url'); ?>/neighbourhood/map"><span>Map</span></a></li>
						</ul>
					</li>
					<li class="menu-item <?php if (is_page('urban-flats')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats"><span>Urban Flats</span></a></li>
					<li class="menu-item <?php if (is_page('gallery')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/gallery"><span>Gallery/Tours</span></a></li>
					<li class="menu-item <?php if (is_page('developer')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/developer"><span>Developer</span></a></li>
					<li class="menu-item <?php if (is_page('contact')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/contact"><span>Contact</span></a></li>
				</ul>
		</div><!-- navigtion -->
		<div id="hamburger">
				<ul>
					<li class="menu-item <?php if (is_page('venue')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/venue"><span>Venue</span></a></li>
					<li class="menu-item <?php if (is_page('history')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/history"><span>History</span></a>
						<ul class="sub-menu" style="display: none;">
							<li><a href="<?php bloginfo('url'); ?>/history/past"><span>Past</span></a></li>
							<li><a href="<?php bloginfo('url'); ?>/history/present"><span>Present</span></a></li>
							<li><a href="<?php bloginfo('url'); ?>/history/future"><span>Future</span></a></li>
						</ul>
					</li>
					<li class="menu-item <?php if (is_page('neighbourhood')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/neighbourhood"><span>Neighbourhood</span></a>
						<ul class="sub-menu" style="display: none;">
							<li><a href="<?php bloginfo('url'); ?>/neighbourhood/quattro"><span></span>Quattro</a></li>
							<li><a href="<?php bloginfo('url'); ?>/neighbourhood/map"><span></span>Map</a></li>
						</ul>
					</li>
					<li class="menu-item <?php if (is_page('urban-flats')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats"><span>Urban Flats</span></a></li>
					<li class="menu-item <?php if (is_page('gallery')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/gallery"><span>Gallery/Tours</span></a></li>
					<li class="menu-item <?php if (is_page('developer')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/developer"><span>Developer</span></a></li>
					<li class="menu-item <?php if (is_page('contact')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/contact"><span>Contact</span></a></li>
				</ul>
		</div>
	</div><!-- header -->

