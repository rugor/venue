<div id="footer_v2" <?php /*if ( is_page('neighbourhood') || is_page('builder') || is_page('register') || is_front_page() ) { ?>class="orange_footer"<?php } */?>>
	<div id="inner_footer">
	<a href="<?php bloginfo('url'); ?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.min.svg" id="footer_logo" alt="Ten Oaks"/></a>
	
	<div class="contact_divs footer_divs footer_divs_wide">
		<p>Presentation Centre:</p>
		<p><a href="https://goo.gl/maps/gLjf9Ke7DxA2" target="_blank">114 – 10768 WHALLEY BOULEVARD</a></p>
	</div><!-- contact divs -->

	<div class="contact_divs footer_divs">
		<p>Telephone:</p>
		<p><a href="tel:604.581.8000">604.581.8000</a></p>
	</div><!-- contact divs -->

	<div class="contact_divs footer_divs">
		<ul class="icons share">
            <li class="fb"><a href="http://facebook.com/venuehomes/" target="_blank" title="Share It"><i class="fa fa-facebook-square fa-2x"></i></a></li>
            <li class="ins"><a href="http://instagram.com/venuehomes/" target="_blank" title="Gram It"><i class="fa fa-instagram fa-2x"></i></a></li>
            <!-- <li class="tw"><a href="#" target="_blank" title="tweet It"><i class="fa fa-twitter fa-2x"></i></a></li> -->
        </ul>
        <!-- .icons share -->
	</div>
		
	<div style="clear: both;"></div>
	<div id="footer_disclaimer">
		<p>In our continuing effort to improve and maintain the high standard of the Venue development, the developer reserves the right to modify or change plans, specifications, features and prices without notice. Materials may be substituted with equivalent or better at the developer's sole discretion. All dimensions and sizes are approximate and are based on preliminary survey measurements. As reverse plans occur throughout the development please see architectural plans. Renderings are an artist’s conception and are intended as a general reference only. E. &amp; O. E.
		</p>
	</div><!-- footer disclaimer -->
	</div><!-- inner footer -->
</div><!-- footer -->

<?php wp_footer(); ?>

<?php if ( is_page ( 'neighbourhood' ) ) { ?>

<script>
	jQuery(function ($) {
	   $('.open-album').click(function(e) {
		    var el, id = $(this).data('open-id');
		    if(id){
		        el = $('.fancybox[rel=' + id + ']:eq(0)');
		        e.preventDefault();
		        el.click();
		    }
		}); 
	});
</script>

<?php } ?>

<?php if ( is_page ( array( 'past', 'present', 'future' ) ) ) { ?>

<script>
	jQuery(function ($) {
	   $('.open-album').click(function(e) {
		    var el, id = $(this).data('open-id');
		    if(id){
		        el = $('.fancybox[rel=' + id + ']:eq(0)');
		        e.preventDefault();
		        el.click();
		    }
		});  
	});
</script>

<?php } ?>

<?php if ( is_page ( 'gallery' ) ) { ?>

<script>
	jQuery(function ($) {
	   $('.open-album').click(function(e) {
		    var el, id = $(this).data('open-id');
		    if(id){
		        el = $('.fancybox[rel=' + id + ']:eq(0)');
		        e.preventDefault();
		        el.click();
		    }
		});  

	   $('.open-vid').click(function(e) {
		    var el, id = $(this).data('open-id');
		    if(id){
		        el = $('.fancybox_video');
		        e.preventDefault();
		        el.click();
		    }
		}); 
	});
</script>

<?php } ?>

<?php if ( is_page_template ( 'floorplans-template.php' ) ) { ?>

<script src='//cdn.rawgit.com/nolimits4web/Swiper/master/dist/js/swiper.min.js'></script>
<script>
  var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    keyboardControl: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    // spaceBetween: 30,
    loop: true,
    lazyLoading	: true,
    preventClicks: false,
    hashnav: true/*,
    paginationBulletRender: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
    }*/
  });
</script>

<?php } ?>

<?php if ( is_page('contact') ) { ?>



<!---   ONLINE FORM VALIDATION SCRIPT   -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/bower_components/jquery-validation/dist/additional-methods.min.js"></script>

<script>
	    $(document).ready(function() {

        $(".scroll").click(function(event) {
            event.preventDefault();
            var full_url = this.href;
            var parts = full_url.split("#");
            var trgt = parts[1];
            var target_offset = $("#" + trgt).offset();
            var target_top = target_offset.top;

            $('html, body').animate({
                scrollTop: target_top
            }, 500);
        });

        /* enable form validation */
        $("#form").validate({
            submitHandler: function(form) {
                form.submit();
            },
            errorLabelContainer: $('#errorContainer'),
            errorClass: 'error',
            rules: {
                FirstName: "required",
                LastName: "required",
                "Phones[Home]": {
                    required: true,
                    phoneUS: true
                },
                "Emails[Primary]": {
                    required: true,
                    email: true
                },
                "ContactPreference": "required",
                "Questions[38192]": "required",
                "opt-in": "required"
            },
            messages: {
                FirstName: "Please enter a first name",
                LastName: "Please enter a last name",
                "Phones[Home]": {
                    required: "Please enter a phone number",
                    email: "Phone number format is not valid"
                },
                "Emails[Primary]": {
                    required: "Please enter an email address",
                    email: "Email address format is not valid"
                },
                "ContactPreference": "Please enter how you you would like to be contacted",
                "Questions[38192]": "Please enter how you heard about us",
                "opt-in": "Please opt in"
            },
            focusInvalid: false,
            invalidHandler: function(form, validator) {

                if (!validator.numberOfInvalids())
                    return;

                $('html, body').animate({
                    scrollTop: $('#errorContainer').offset().top
                }, 500);

                $(validator.errorList[0].element).focus();

            }
        });
    });

</script>
<script type="text/javascript">
var _ldstJsHost = (("https:" == document.location.protocol) ? "https://" : "http://");
_ldstJsHost += "www.mylasso.com";
document.write(unescape("%3Cscript src='" + _ldstJsHost + "/analytics.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
<!‐‐
var LassoCRM = LassoCRM || {};
(function(ns){
ns.tracker = new LassoAnalytics('LAS‐274365‐01');
})(LassoCRM);
try {
LassoCRM.tracker.setTrackingDomain(_ldstJsHost);
LassoCRM.tracker.init(); // initializes the tracker
LassoCRM.tracker.track(); // track() records the page visit with the current page title, to record multiple visits call
repeatedly.
} catch(error) {}
‐‐>
</script>
<script type="text/javascript">
(function(i) {var u =navigator.userAgent;
var e=/*@cc_on!@*/false; var st = setTimeout;if(/webkit/i.test(u)){st(function(){var dr=document.readyState;
if(dr=="loaded"||dr=="complete"){i()}else{st(arguments.callee,10);}},10);}
else if((/mozilla/i.test(u)&&!/(compati)/.test(u)) || (/opera/i.test(u))){
document.addEventListener("DOMContentLoaded",i,false); } else if(e){ (
function(){var t=document.createElement("doc:rdy");try{t.doScroll("left");
i();t=null;}catch(e){st(arguments.callee,0);}})();}else{window.onload=i;}})
(function() {document.forms[0].guid.value = LassoCRM.tracker.readCookie("ut");});
</script>
<?php } ?>
</body>
</html>