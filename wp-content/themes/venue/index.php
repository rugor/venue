<?php include('header.php'); ?>

<?php if ( is_page ( 'venue' ) ) { ?>

<div class="wrap" style="position: relative; width: 100%;">
	
	<div class="group-selfie"></div>

	<div id="venue_content" class="page-content">

			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<img src="<?php bloginfo('template_directory'); ?>/images/patio.png" alt="">
						<div class="abs">
							<img src="<?php bloginfo('template_directory'); ?>/images/party.png" alt="">
						</div>
					</div>
				</div><!-- /row -->
				<div class="row">
					<div class="col-md-6 col-md-offset-1">
						<h3>Our time is here</h3>
						<p>The Whalley neighbourhood has had its ups and downs, but like many historic Vancouver neighbourhoods that have been revitalized – <span>YALETOWN, Chinatown, Main Street, Commercial Drive</span> – its time has come.</p>
						<h4>be part of the change</h4>
					</div>
					<div class="col-md-4 under-image"></div>
				</div><!-- row -->
				<div class="row">
					<div class="col-md-6 under-image2"></div>
					<div class="col-md-6"><p>Early buyers in Vancouver’s revitalized communities saw their neighbourhoods grow into thriving, cultural hotspots where real estate prices rose with the areas’ desirability. For some, it was an investment. For others, it was a chance to have the authentic urban lifestyle they craved, in a home of their own, and in a neighbourhood they could help define. Now, it’s happening in Whalley – and Venue is your chance to be part of the change. Just a 5-minute walk from the Gateway SkyTrain station, it has the perks you want (like a rooftop lounge with an outdoor TV and fireplace, plus a gym) with a future you can buy into. Venue – it’s the moment you’ve been waiting for.</p></div>
				</div><!-- /row -->
			</div><!-- /container-fluid -->

	</div>
</div>

	
<?php } ?>

<?php if ( is_page ( 'quattro' ) ) { ?>

<div class="wrap" style="position: relative; width: 100%;">

	<div id="quattro_content" class="page-content">

		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h3>Home to Trailblazers</h3>
					<img src="<?php bloginfo('template_directory'); ?>/images/site.png" alt="">
				</div>
			</div><!-- /row -->
			<div class="row">
				<div class="col-md-5 col-md-offset-1">
					<p>Tien Sher began developing the Quattro community where Venue is located in 2006. Five hundred new homes – ranging from condos to micro-suites – brought new life to a vacant lot and fresh faces to the established, residential neighbourhood. Now, ten years later, Venue is the last development in Quattro. It’s a neighbourhood on the edge of change, just a five-minute walk from the Gateway SkyTrain station and a 30-minute ride from downtown Vancouver. </p>
				</div>
				<div class="col-md-5">
					<p>It’s a neighbourhood for nostalgia buffs and adventurers: retro diners, ethnic food marts and restaurants, old-time barber shops, bowling alleys and roller rinks.  And it’s a neighbourhood for those who like to have fun: brewpubs, community gardens, summer farmers’ markets, Chuck Bailey Rec Centre sports courts, BC Lions practices and Whalley Little League games. And for those other things even trailblazers need? There’s City Centre, just one minute by SkyTrain.</p>
				</div>
			</div><!-- /row -->
		</div><!-- /container-fluid -->

	</div><!-- /page-content -->
</div><!-- /wrap -->
	
<?php } ?>

<?php if ( is_page ( 'past' ) ) {

$content = get_field('content');
$gallery_images = get_field('gallery_images');

?>

<div id="history_content" class="page-content">

	<div class="gaspump"></div>

	<div class="container-fluid">

		<div class="row">
			<div class="col-md-6 col-lg-5">
				<p>Whalley began in the early 1920’s as a three-acre triangle of land at what would later be the intersection of 108 Avenue, Grosvenor Rd. and the King George Hwy. The owner, an enterprising bootlegger named Arthur Whalley, anticipated the need for gas and services on the route between New Westminster and the U.S. border, so he built a service station with a small general store that became known as “Whalley’s Corner”.</p>
			</div>
			<div class="col-md-6 col-lg-5">
				<h2><span>Whalley</span><br/>History</h2>
				<img src="<?php bloginfo('template_directory'); ?>/images/gastation.png" alt="">
			</div>
		</div><!-- /row -->

		<div class="row">
			<div class="col-md-12">
				<h3>Where it all started</h3>
			</div>
		</div><!-- /row -->

		<div class="row">
			<div class="col-md-5">
				<p>Settlement in the area increased in the late 1930’s with the completion of the Pattullo Bridge, a major water main and the King George Hwy. In 1948, the area was officially named “Whalley” (“Corner” was deemed “too unsophisticated”). In the 1950’s and 60’s, after the bridge toll was removed, and as the Port Mann Bridge and Highway 1 were completed, Whalley experienced a major building boom and it has continued to be one of the fastest growing, most densely populated districts in Surrey</p>
			</div><!-- /row -->
			<div class="col-md-6"><img src="<?php bloginfo('template_directory'); ?>/images/bridge.png" alt=""></div>
			<div class="col-md-1"></div>
		</div><!-- /row -->

		<div class="row">
			<div class="col-md-12 timeline-btn">
				<button class="view-timeline open-album" data-open-id="gallery">
					View the timeline
				</button>
			</div>
		</div><!-- /row -->

	</div><!-- /container-fluid -->
</div><!-- /page-content -->

	<div id="gallery_grid" style="display:none">
		<div class="grid-sizer"></div>
		<?php foreach($gallery_images as $image) {
			if ( $image['video_link'] ) { 
				echo '<a href="' . $image['video_link'] . '" class="fancybox_video image-show" data-fancybox-type="iframe" rel="gallery"><div style="background: url(' . $image['image'] . ') center center no-repeat;" class="grid-item ' . $image['image_size'] . '"><div class="vid_overlay"><span></span><p>' . $image['video_title'] . '</p></div></div></a>';
			} else {
				echo '<a href="' . $image['image'] . '" class="fancybox image-show" rel="gallery"><div style="background: url(' . $image['image'] . ') center center no-repeat;" class="grid-item ' . $image['image_size'] . '"></div></a>';
			}
		} ?>
	</div><!-- gallery grid -->
	<div class="clear"></div>

<?php } ?>

<?php if ( is_page ( 'present' ) ) {

$content = get_field('content');
$gallery_images = get_field('gallery_images');

?>

<div id="history_content" class="page-content">
	
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-6 col-lg-5">
				<p>Today, Whalley is the focus of significant development. With a strong transportation network that provides easy access to the rest of Metro Vancouver, an affordable housing market, established shopping malls, and a wealth of parks (Green Timbers Urban Forest, Bear Creek Park and Holland Park), the culturally diverse area is poised for change. The City of Surrey’s plan to develop the City Centre area in which Venue is located into the region’s main business, cultural and activity centre is well underway and Tien Sher is proud to be part of the change. In 2006, we began building the Quattro neighbourhood where Venue is located and we’re delighted to see old favourites such as the BC Lions training facility and Whalley Little League rediscovered by new residents, along with all the new additions to the neighbourhood.</p>
			</div>
			<div class="col-md-6 col-lg-6">
				<h2><span>Whalley</span><br/>Now</h2>
				<img src="<?php bloginfo('template_directory'); ?>/images/surrey-sfu.png" alt="">
				<img class="render" src="<?php bloginfo('template_directory'); ?>/images/surrey-render.png" alt="">
			</div>
		</div><!-- /row -->

		<div class="row">
			<div class="col-md-7">
				<h3>An Historic district on the edge of change</h3>
			</div>
		</div><!-- /row -->

		<div class="row">
			<div class="col-md-5">
				<p>City Centre is home to an SFU campus, a shopping centre with more than 140 stores and services, a new library and city hall – as well as the newly expanded Surrey Memorial hospital, RCMP headquarters, Chuck Bailey Rec Centre and two SkyTrain stations (Gateway and Surrey Central).</p>
			</div>
			<div class="col-md-6"><img src="<?php bloginfo('template_directory'); ?>/images/quattro.png" alt=""></div>
		</div><!-- /row -->

		<div class="row">
			<div class="col-md-12 timeline-btn">
				<button class="view-timeline open-album" data-open-id="gallery">
					View the timeline
				</button>
			</div>
		</div><!-- /row -->

	</div><!-- /container-fluid -->

</div><!-- /page-content -->

	<div id="gallery_grid" style="display:none">
		<div class="grid-sizer"></div>
		<?php foreach($gallery_images as $image) {
			if ( $image['video_link'] ) { 
				echo '<a href="' . $image['video_link'] . '" class="fancybox_video image-show" data-fancybox-type="iframe" rel="gallery"><div style="background: url(' . $image['image'] . ') center center no-repeat;" class="grid-item ' . $image['image_size'] . '"><div class="vid_overlay"><span></span><p>' . $image['video_title'] . '</p></div></div></a>';
			} else {
				echo '<a href="' . $image['image'] . '" class="fancybox image-show" rel="gallery"><div style="background: url(' . $image['image'] . ') center center no-repeat;" class="grid-item ' . $image['image_size'] . '"></div></a>';
			}
		} ?>
	</div><!-- gallery grid -->
	<div class="clear"></div>

<?php } ?>

<?php if ( is_page ( 'future' ) ) {

$content = get_field('content');
$gallery_images = get_field('gallery_images');

?>
<div class="wrap" style="position: relative; width: 100%;">
	
	<div class="building"></div>
	
	<div id="history_content" class="page-content">
		
		<div class="container-fluid">
			
			<div class="row">
				<div class="col-md-6 col-lg-5">
					<p>As the City of Surrey continues to develop Whalley, and the City Centre area in particular, into a major business, cultural and activity centre, new projects such as a 1,600-seat performing arts centre and a Kwantlen Polytechnic University campus will attract even more of the area’s youthful population. They see Whalley’s storied past as the foundation for the next generation of change. Retro diners, ethnic food marts and restaurants, old-time barber shops, bowling alleys and roller rinks are as much a destination as the growing selection of bistros, brewpubs, farmer’s markets and community gardens. </p>
				</div>
				<div class="col-md-6 col-lg-6">
					<h2><span>What's</span><br/>Next</h2>
					<img src="<?php bloginfo('template_directory'); ?>/images/bank.png" alt="">
				</div>
			</div><!-- /row -->
			
			<div class="row">
				<div class="col-md-10">
					<h3>metro’s next urban core</h3>
				</div>
			</div><!-- /row -->
			
			<div class="row">
				<div class="col-md-5">
					<p>By 2031, the population of City Centre is expected to double as it transforms into a vibrant, walkable, urban downtown core – second only to Vancouver. Tien Sher will once again be part of the change with a community of high-rise towers and low-rise apartment buildings slated for future development in the 	Venue neighbourhood.</p>
				</div>
				<div class="col-md-6"><img src="<?php bloginfo('template_directory'); ?>/images/pond.png" alt=""></div>
			</div><!-- /row -->
			
			<div class="row">
				<div class="col-md-12 timeline-btn">
					<button class="view-timeline open-album" data-open-id="gallery">
						View the timeline
					</button>
				</div>
			</div><!-- /row -->
		
		</div><!-- /container-fluid -->
	</div><!-- /page-content -->

	<div id="gallery_grid" style="display:none">
		<div class="grid-sizer"></div>
		<?php foreach($gallery_images as $image) {
			if ( $image['video_link'] ) { 
				echo '<a href="' . $image['video_link'] . '" class="fancybox_video image-show" data-fancybox-type="iframe" rel="gallery"><div style="background: url(' . $image['image'] . ') center center no-repeat;" class="grid-item ' . $image['image_size'] . '"><div class="vid_overlay"><span></span><p>' . $image['video_title'] . '</p></div></div></a>';
			} else {
				echo '<a href="' . $image['image'] . '" class="fancybox image-show" rel="gallery"><div style="background: url(' . $image['image'] . ') center center no-repeat;" class="grid-item ' . $image['image_size'] . '"></div></a>';
			}
		} ?>
	</div><!-- gallery grid -->
	<div class="clear"></div>
</div>
<?php } ?>

<?php if ( is_page ( 'developer' ) ) { ?>

    <div id="developer_content" class="page-content">
        <div class="container-fluid">
    
		    <h3>From our family to yours</h3>
		    
		    <div class="row">
		        <div class="col-md-8 col-md-offset-2">
		            <p>Tien Sher Homes, named for the “three lions” as represented by founder Charan Sethi and his two sons, is a symbol for honesty, integrity, and quality. Since 2005, Charan has assembled a team of experienced professionals to drive the company forward in our quest to develop strong, thriving communities. Our close-knit ensemble of dedicated staff work tirelessly to ensure that each and every home we build has equal amounts of creativity and heart. Charan’s philosophy challenges the entire team to treat each and every condominium and townhome that carries the Tien Sher brand as if we were building it for ourselves. It’s because of this type of leadership that Charan was nominated Business Man of the Year by the Surrey Board of Trade.</p>
		            <blockquote style="width: 80%; margin: 20px auto 0;">
		                <p>“We may be in the bricks and mortar industry, but at the end of the day, our business is about people, first and foremost.”
		                    <footer>Charan Sethi</footer>
		                </p>
		            </blockquote>
		        </div><!-- /col-md-8 col-md-offset-2" -->
		    </div><!-- /row -->
		    
		    <div class="row">
		        <div class="col-md-8 col-md-offset-2">
		        	<h4>What?</h4>
		            <p>As a fully-integrated real estate development company, the highly-focused team of specialists at Tien Sher Homes oversees the acquisition, planning, design, construction, marketing, sales, and customer care of a broad spectrum of residential developments across the Lower Mainland. But we don’t stop there. We also support local charitable organizations, and host events to ensure our communities are much more than mere assemblages of dwellings. We roll up our sleeves to help clean up our streets, organizing volunteers and launching clean-up programs along the way. In short, we care about the communities that we build in.</p>
		            <blockquote>
		                <p>“We’re still a growing company, so we have to try harder.”</p>
		            </blockquote>
		        </div><!-- /col-md-8 col-md-offset-2 -->
		    </div><!-- /row -->
		    
		    <div class="row">
		        <div class="col-md-8 col-md-offset-2">
		        	<h4>Why?</h4>
		            <p>Our work means a great deal to us, and it’s because we’ve seen the excitement in the eyes of every young couple who receives the keys to their home, the smiles of children as they walk through the door to their new bedroom. These are moments to be treasured, because we’ve been there ourselves, and we understand the significance of every such moment. We provide more than safe shelter. We offer more than a quiet retreat. Every house we build is a home that’s been conceived, designed, constructed, and will be cared for as though we would be living in them. And that is why every Tien Sher home comes from our family to yours.</p>
		        </div><!-- /col-md-8 col-md-offset-2 -->
		    </div><!-- /row -->
		    <img class="tien-sher" src="<?php bloginfo('template_directory'); ?>/images/tien-sher.svg" alt="">
		</div><!-- /rcontainer-fluid -->
    </div><!-- /page-content -->

<?php } ?>

<?php include('footer.php'); ?>