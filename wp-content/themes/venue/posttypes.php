<?php
/*
*
* @package _p-fo
*
* Modified to include only 'Projects'. Other post-types can be included by uncommenting.
*
*/


add_action( 'init', 'marca_fp_manager_init' );

function marca_fp_manager_init() {
    register_post_type( 'marca_fp_manager',
        array(
            'labels' => array(
                'name' => 'Floorplans',
                'singular_name' => 'Floorplan',
                'add_new' => 'Add New Floorplan',
                'add_new_item' => 'Add New Floorplan',
                'edit' => 'Edit',
                'edit_item' => 'Edit Floorplan',
                'new_item' => 'New Floorplan',
                'view' => 'View',
                'view_item' => 'View Floorplan',
                'search_items' => 'Search Floorplans',
                'not_found' => 'No Floorplans found',
                'not_found_in_trash' => 'No Floorplans found in Trash',
                'parent' => 'Parent Floorplan'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title'),
            'taxonomies' => array( '' ),
            'menu_icon' => 'dashicons-admin-generic',
            'has_archive' => true
        )
    );
    register_taxonomy_for_object_type('post_tag', 'marca_fp_manager');
    // register_taxonomy_for_object_type('category', 'marca_fp_manager');
}


// Add new post type for Projects
add_action('init', 'marca_fp_manager_init');
function marca_fp_manager_init() 
{
	$work_labels = array(
		'name' => _x('Works', 'post type general name'),
		'singular_name' => _x('Work', 'post type singular name'),
		'all_items' => __('All Works'),
		'add_new' => _x('Add new work', 'works'),
		'add_new_item' => __('Add new work'),
		'edit_item' => __('Edit work'),
		'new_item' => __('New work'),
		'view_item' => __('View work'),
		'search_items' => __('Search in works'),
		'not_found' =>  __('No works found'),
		'not_found_in_trash' => __('No works found in trash'), 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $work_labels,
		'public' => true,
		'menu_icon' => 'dashicons-hammer',
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
		'has_archive' => 'works'
	); 
	register_post_type('marca_fp_manager',$args);
	//register_taxonomy_for_object_type('post_tag', 'works');
	register_taxonomy_for_object_type('category', 'works');
}

// Add new post type for Publications
add_action('init', 'publications_init');
function publications_init() 
{
	$publication_labels = array(
		'name' => _x('Publications', 'post type general name'),
		'singular_name' => _x('Publication', 'post type singular name'),
		'all_items' => __('All Publications'),
		'add_new' => _x('Add new publication', 'publications'),
		'add_new_item' => __('Add new publication'),
		'edit_item' => __('Edit publication'),
		'new_item' => __('New publication'),
		'view_item' => __('View publication'),
		'search_items' => __('Search in publications'),
		'not_found' =>  __('No publications found'),
		'not_found_in_trash' => __('No publications found in trash'), 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $publication_labels,
		'public' => true,
		'menu_icon' => 'dashicons-book-alt',
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 8,
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
		'has_archive' => 'publications'
	); 
	register_post_type('publications',$args);
	// register_taxonomy_for_object_type('post_tag', 'publications');
	register_taxonomy_for_object_type('category', 'publications');
}

// Add new post type for Press Items
add_action('init', 'press_init');
function press_init() 
{
	$press_labels = array(
		'name' => _x('Press', 'post type general name'),
		'singular_name' => _x('Press', 'post type singular name'),
		'all_items' => __('All Press'),
		'add_new' => _x('Add new press', 'press'),
		'add_new_item' => __('Add new press'),
		'edit_item' => __('Edit press'),
		'new_item' => __('New press'),
		'view_item' => __('View press'),
		'search_items' => __('Search in press'),
		'not_found' =>  __('No press found'),
		'not_found_in_trash' => __('No press found in trash'), 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $press_labels,
		'public' => true,
		'menu_icon' => 'dashicons-id-alt',
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
		'has_archive' => 'press'
	); 
	register_post_type('press',$args);
	// register_taxonomy_for_object_type('post_tag', 'press');
	register_taxonomy_for_object_type('category', 'press');
}
/*
// Add new post type for Exhibitions

add_action('init', 'select_works_init');
function select_works_init() 
{
	$selected_works_labels = array(
		'name' => _x('Selected Works', 'post type general name'),
		'singular_name' => _x('Selected Work', 'post type singular name'),
		'all_items' => __('All Selected Works'),
		'add_new' => _x('Add new Selected Work', 'selected works'),
		'add_new_item' => __('Add new Selected Work'),
		'edit_item' => __('Edit Selected Work'),
		'new_item' => __('New Selected Work'),
		'view_item' => __('View Selected Work'),
		'search_items' => __('Search in Selected Work'),
		'not_found' =>  __('No Selected Works found'),
		'not_found_in_trash' => __('No Selected Works found in trash'), 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $selected_works_labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
		'has_archive' => 'selected-works'
	); 
	
	register_post_type('selected-works',$args);
	register_taxonomy_for_object_type('post_tag', 'selected-works');
	register_taxonomy_for_object_type('category', 'selected-works');
}*/
/*
// Add new post type for Products
add_action('init', 'product_init');
function product_init() 
{
	$product_labels = array(
		'name' => _x('Products', 'post type general name'),
		'singular_name' => _x('Product', 'post type singular name'),
		'all_items' => __('All Products'),
		'add_new' => _x('Add new product', 'products'),
		'add_new_item' => __('Add new product'),
		'edit_item' => __('Edit product'),
		'new_item' => __('New product'),
		'view_item' => __('View product'),
		'search_items' => __('Search in products'),
		'not_found' =>  __('No products found'),
		'not_found_in_trash' => __('No products found in trash'), 
		'parent_item_colon' => ''
	);
	$args = array(
		'labels' => $product_labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'supports' => array('title','editor','author','thumbnail','excerpt','comments','custom-fields'),
		'has_archive' => 'products'
	);
	 
	register_post_type('products',$args);
	register_taxonomy_for_object_type('post_tag', 'products');
	register_taxonomy_for_object_type('category', 'products');
}*/