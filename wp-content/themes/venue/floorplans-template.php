<?php include('header.php'); 

/* Template Name: Floorplans Template */

?>

<div id="sub-navigation">
		<ul id="sub-menu" class="unit-types">
			<li class="menu-item <?php if (is_page('studio')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats/studio"><span>Studio</span></a></li>
			<li class="menu-item <?php if (is_page('1-bed')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats/1-bed"><span>1 Bed</span></a></li>
			<li class="menu-item <?php if (is_page('1-bed-den')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats/1-bed-den"><span>1 Bed and Den</span></a></li>
			<li class="menu-item <?php if (is_page('2-bed')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats/2-bed"><span>2 Bed</span></a></li>
			<li class="menu-item <?php if (is_page('2-bed-den')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats/2-bed-den"><span>2 Bed and Den</span></a></li>
			<!-- <li class="menu-item <?php if (is_page('features')) { ?>current-menu-item <?php } ?>"><a href="<?php bloginfo('url'); ?>/urban-flats/features"><span>Features</span></a></li> -->
		</ul>
</div><!-- navigtion -->

<?php if ( is_page('urban-flats') ) { 

	$planTypes = array();
	$planArr = array();

		$args = array(
			'post_type' => 'marca_fp_manager',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => "-1"
		);
		
		$loop = new WP_Query($args);
		
		while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$theSeries = get_field('series');
		
		if ( !in_array($theSeries, $planTypes) ) {
			$planTypes[] = $theSeries;
		}
		
		$planArr[] = array("type" => get_field('type'), "lower" => get_field('lower_sqft'), "main" => get_field('main_sqft'), "upper" => get_field('upper_sqft'), "image" => get_field('plan_image'),"logo" => get_field('plan_logo'), "series" => get_field('series'), "PDF" => get_field('plan_pdf'), "ranger" => get_field('size_range'));
		
		// select box label
		$field = get_field_object('type');
		$value = get_field('type');
		$label = $field['choices'][ $value ];

		endwhile;
		wp_reset_postdata();

?>

<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $label; ?></p>		
									
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->


<?php } ?>

<?php if ( is_page('studio') ) { 

	$planTypes = array();
	$planArr = array();

		$args = array(
			'post_type' => 'marca_fp_manager',
			'meta_key'		=> 'type',
			'meta_value'	=> 'studio',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => "-1"
		);
		
		$loop = new WP_Query($args);
		
		while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$theSeries = get_field('series');
		
		if ( !in_array($theSeries, $planTypes) ) {
			$planTypes[] = $theSeries;
		}
		
		$planArr[] = array("type" => get_field('type'), "lower" => get_field('lower_sqft'), "main" => get_field('main_sqft'), "upper" => get_field('upper_sqft'), "image" => get_field('plan_image'),"logo" => get_field('plan_logo'), "series" => get_field('series'), "PDF" => get_field('plan_pdf'), "ranger" => get_field('size_range'));

		// select box label
		$field = get_field_object('type');
		$value = get_field('type');
		$label = $field['choices'][ $value ];
		
		endwhile;
		wp_reset_postdata();

?>

<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $label; ?></p>
									
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->

<?php } ?>
<?php if ( is_page('1-bed') ) { 
	$planTypes = array();
	$planArr = array();

		$args = array(
			'post_type' => 'marca_fp_manager',
			'meta_key'		=> 'type',
			'meta_value'	=> '1-bed',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => "-1"
		);
		
		$loop = new WP_Query($args);
		
		while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$theSeries = get_field('series');
		
		if ( !in_array($theSeries, $planTypes) ) {
			$planTypes[] = $theSeries;
		}
		
		$planArr[] = array("type" => get_field('type'), "lower" => get_field('lower_sqft'), "main" => get_field('main_sqft'), "upper" => get_field('upper_sqft'), "image" => get_field('plan_image'),"logo" => get_field('plan_logo'), "series" => get_field('series'), "PDF" => get_field('plan_pdf'), "ranger" => get_field('size_range'));

		// select box label
		$field = get_field_object('type');
		$value = get_field('type');
		$label = $field['choices'][ $value ];
		
		endwhile;
		wp_reset_postdata();
?>

<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $label; ?></p>
									
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->

<?php } ?>
<?php if ( is_page('1-bed-den') ) { 

	$planTypes = array();
	$planArr = array();

		$args = array(
			'post_type' => 'marca_fp_manager',
			'meta_key'		=> 'type',
			'meta_value'	=> '1-bed-den',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => "-1"
		);
		
		$loop = new WP_Query($args);
		
		while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$theSeries = get_field('series');
		
		if ( !in_array($theSeries, $planTypes) ) {
			$planTypes[] = $theSeries;
		}
		
		$planArr[] = array("type" => get_field('type'), "lower" => get_field('lower_sqft'), "main" => get_field('main_sqft'), "upper" => get_field('upper_sqft'), "image" => get_field('plan_image'),"logo" => get_field('plan_logo'), "series" => get_field('series'), "PDF" => get_field('plan_pdf'), "ranger" => get_field('size_range'));

		// select box label
		$field = get_field_object('type');
		$value = get_field('type');
		$label = $field['choices'][ $value ];
		
		endwhile;
		wp_reset_postdata();

?>

<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $label; ?></p>
									
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->

<?php } ?>
<?php if ( is_page('2-bed') ) { 

	$planTypes = array();
	$planArr = array();

		$args = array(
			'post_type' => 'marca_fp_manager',
			'meta_key'		=> 'type',
			'meta_value'	=> '2-bed',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => "-1"
		);
		
		$loop = new WP_Query($args);
		
		while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$theSeries = get_field('series');
		
		if ( !in_array($theSeries, $planTypes) ) {
			$planTypes[] = $theSeries;
		}
		
		$planArr[] = array("type" => get_field('type'), "lower" => get_field('lower_sqft'), "main" => get_field('main_sqft'), "upper" => get_field('upper_sqft'), "image" => get_field('plan_image'),"logo" => get_field('plan_logo'), "series" => get_field('series'), "PDF" => get_field('plan_pdf'), "ranger" => get_field('size_range'));

		// select box label
		$field = get_field_object('type');
		$value = get_field('type');
		$label = $field['choices'][ $value ];
		
		endwhile;
		wp_reset_postdata();

?>

<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $label; ?></p>
									
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->

<?php } ?>
<?php if ( is_page('2-bed-den') ) { 

	$planTypes = array();
	$planArr = array();

		$args = array(
			'post_type' => 'marca_fp_manager',
			'meta_key'		=> 'type',
			'meta_value'	=> '2-bed-den',
			'orderby' => 'date',
			'order' => 'ASC',
			'posts_per_page' => "-1"
		);
		
		$loop = new WP_Query($args);
		
		while ( $loop->have_posts() ) : $loop->the_post(); 
		
		$theSeries = get_field('series');
		
		if ( !in_array($theSeries, $planTypes) ) {
			$planTypes[] = $theSeries;
		}
		
		$planArr[] = array("type" => get_field('type'), "lower" => get_field('lower_sqft'), "main" => get_field('main_sqft'), "upper" => get_field('upper_sqft'), "image" => get_field('plan_image'),"logo" => get_field('plan_logo'), "series" => get_field('series'), "PDF" => get_field('plan_pdf'), "ranger" => get_field('size_range'));

		// select box label
		$field = get_field_object('type');
		$value = get_field('type');
		$label = $field['choices'][ $value ];
		
		endwhile;
		wp_reset_postdata();

?>

<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $label; ?></p>
									
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->

<?php } ?>

<?php include('footer.php'); ?>