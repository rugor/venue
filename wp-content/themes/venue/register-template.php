<?php include('header.php'); 
/* Template Name: Register Template */
// $content = get_field('content');
?>

<div id="register_content" class="page-content">
	<div class="container-fluid">
            <header>
                <h1 class="register-today">REGISTER TO STAY INFORMED ABOUT VENUE</h1>
            </header>
            <form method="POST" enctype="application/x-www-form-urlencoded" action="http://www.mylasso.com/registrant_signup/" id="form" class="form">
                <fieldset>
                    <div id="errorContainer"></div>
                    <div class="row">
                        <div id="nameTitle" class="col-md-3 input-field form-group dropdown">
                            <label for="NameTitle">Title</label>
                            <select class="form-control" id="NameTitle" name="NameTitle">
                                <option value="" selected></option>
                                <option value="Mr">Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Mr & Mrs">Mr &amp; Mrs</option>
                                <option value="Ms">Ms</option>
                                <option value="Miss">Miss</option>
                                <option value="Dr">Dr</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-9 form-group"></div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="FirstName">First Name<span class="req">*</span></label>
                            <input type="text" name="FirstName" class="form-control" id="FirstName" >
                        </div>
                        <!-- /col-md-6  -->
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="LastName">Last Name<span class="req">*</span></label>
                            <input type="text" name="LastName" class="form-control" id="LastName" >
                        </div>
                        <!-- /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-6 input-field form-group">
                            <label for="phone">Phone Number<span class="req">*</span></label>
                            <input type="text" id="phone" name="Phones[Home]" class="form-control" value="" />
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="Emails[Primary]">Email<span class="req">*</span></label>
                            <input type="text" name="Emails[Primary]" class="form-control" id="Emails[Primary]" >
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-12 input-field form-group">
                            <label class="control-label" for="Address">Address</label>
                            <input type="text" name="Address" class="form-control" id="Address" >
                        </div>
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="City">City</label>
                            <input type="text" name="City" class="form-control" id="City" >
                        </div>
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="Province">Province/State</label>
                            <input type="text" name="Province" class="form-control" id="Province" >
                        </div>
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="PostalCode">Postal Code/Zip</label>
                            <input type="text" name="PostalCode" class="form-control" id="PostalCode" >
                        </div>
                        <div class="col-md-6 input-field form-group">
                            <label class="control-label" for="Country">Country</label>
                            <input type="text" name="Country" class="form-control" id="Country" >
                        </div>
                        <!-- /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group"><label for="ContactPreference">What is your contact preference?<span class="req">*</span></label></div>
                        <div class="col-md-5 input-field form-group dropdown">
                            <select class="form-control" id="ContactPreference" name="ContactPreference">
                                <option value=""></option>
                                <option value="Any" selected>Any</option>
                                <option value="Email">Email</option>
                                <option value="Mail">Mail</option>
                                <option value="No Contact">No Contact</option>
                                <option value="No Email">No Email</option>
                                <option value="Phone">Phone</option>
                                <option value="Text">Text</option>
                            </select>
                        </div>
                        <!-- /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="heard">How did you hear about this community?<span class="req">*</span></label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38192]" id="heard" class="form-control required">
                                <option></option>
                                <option value="176913">Internet</option>
                                <option value="176914">Signage</option>
                                <option value="176915">Friend/Refferal</option>
                                <option value="176916">MLS</option>
                                <option value="176917">Realtor</option>
                                <option value="176918">New Home Buyers Guide</option>
                                <option value="176919">Flyer/Mail</option>
                                <option value="176920">Sky Train</option>
                                <option value="176921">The Province</option>
                                <option value="176922">The Vancouver Sun</option>
                                <option value="176923">Ming Pao/Sing Tao</option>
                                <option value="176924">Surrey Leader</option>
                                <option value="176925">The Metro</option>
                                <option value="176926">24 Hours</option>
                                <option value="176927">Real Estate Guide</option>
                                <option value="176928">Burnaby Now</option>
                                <option value="176929">New West Leader</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="interest">Are you purchasing for...</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38185]" id="interest" class="form-control">
                                <option></option>
                                <option value="176889">Primary Residence</option>
                                <option value="176890">Investment</option>
                                <option value="176891">For Family to live in</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="move-in">How many bedrooms are you looking for?</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38180]" id="move-in" class="form-control">
                                <option></option>
                                <option value="176867">Studio</option>
                                <option value="176868">1 Bedroom</option>
                                <option value="176869">1 Bedroom Plus Den</option>
                                <option value="176870">2 Bedroom</option>
                                <option value="176871">2 Bedroom + Den</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="price">What is your desired price range?</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38183]" id="price" class="form-control">
                                <option></option>
                                <option value="176880">Under $200,000</option>
                                <option value="176881">$200,000 – $250,000</option>
                                <option value="176882">$250,000 – $300,000</option>
                                <option value="176883">$300,000 +</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="price">When would you like to purchase?</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38184]" id="when" class="form-control">
                                <option></option>
                                <option value="176884">Immediately</option>
                                <option value="176885">3 – 6 Months</option>
                                <option value="176886">6 – 12 Months</option>
                                <option value="176887">More than 12 months</option>
                                <option value="176888">Not applicable</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="age">What is your age group?</label>
                        </div>
                        <!-- /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38190]" id="age" class="form-control">
                                <option></option>
                                <option value="176907">Twenties</option>
                                <option value="176908">Thirties</option>
                                <option value="176909">Fourties</option>
                                <option value="176910">Fifties</option>
                                <option value="176911">Sixties</option>
                                <option value="176912">Seventies or better</option>
                            </select>
                        </div>
                        <!-- /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="living-status">What is your living status?</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38188]" id="living-status" class="form-control">
                                <option></option>
                                <option value="176898">Own</option>
                                <option value="176899">Rent</option>
                                <option value="176900">Living with parents</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="living-status">Are you...</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group dropdown">
                            <select name="Questions[38189]" id="living-status" class="form-control">
                                <option></option>
                                <option value="176901">Single</option>
                                <option value="176902">Couple</option>
                                <option value="176903">Couple + Kids</option>
                                <option value="176904">Single + Kids</option>
                                <option value="176905">Retired</option>
                                <option value="176906">Other</option>
                            </select>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="realtor">Are you a realtor?</label>
                            <!--  /realtor  -->
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group">
                            <div class="realtor">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" value="176896" id="realtor1" name="Questions[38187]">
                                    <label for="realtor1"> Yes </label>
                                </div>
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" value="176897" id="realtor2" name="Questions[38187]">
                                    <label for="realtor2"> No</label>
                                </div>
                            </div>
                            <!--  /realtor  -->
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-7 form-group">
                            <label for="realtor-working">Are you working with a realtor?</label>
                            <!--  /realtor-working  -->
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-5 input-field form-group">
                            <div class="realtor-working">
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" value="176892" id="realtor-working-1" name="Questions[38186]">
                                    <label for="realtor-working-1"> Yes </label>
                                </div>
                                <div class="radio radio-success radio-inline">
                                    <input type="radio" value="176893" id="realtor-working-2" name="Questions[38186]">
                                    <label for="realtor-working-2"> No</label>
                                </div>
                            </div>
                            <!--  /realtor-working  -->
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-2 form-group">
                            <label for="comments">Comments:</label>
                        </div>
                        <!--  /col-md-6  -->
                        <div class="col-md-10 input-field form-group">
                            <textarea name="Comments" rows="4" class="form-control"></textarea>
                        </div>
                        <!--  /col-md-6  -->
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="checkbox checkbox-success opt-in">
                                <input name="opt-in" id="opt-in" value="Yes" class="required no-error-block" type="checkbox">
                                <label for="opt-in" style="padding-bottom: 0;">YES, I CONSENT TO RECEIVING EMAILS.<span class="req">*</span></label>
                            </div>
                        </div>
                        <!--  /col-md-12  -->
                    </div>
                    <!-- <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8 form-group">
                            <div class="checkbox checkbox-success opt-in">
                                <input name="opt-in" id="opt-in" value="Yes" class="required no-error-block" type="checkbox">
                                <label for="opt-in" style="padding-bottom: 0;">YES, I CONSENT TO RECEIVING EMAILS.<span class="req">*</span></label>
                            </div>
                        </div>
                    </div> -->
                    <input type="hidden" name="ClientID" value="940">
                    <input type="hidden" name="projectIds[]" value="5249">
                    <input type="hidden" name="LassoUID" value="D5nAt6wVc}">
                    <input type="hidden" name="SignupThankyouLink" value="http://venuelife.ca/thank-you" />
                    <input type="hidden" name="SignupEmailLink" value="http://venuelife.ca/thankyou-email.html" />
                    <input type="hidden" name="SignupEmailSubject" value="Thank you for registering at [@PROJECT_NAME]." />

                    <input type="hidden" name="domainAccountId" value="LAS‐274365‐01" />
                    <input type="hidden" name="guid" value="" />

                    <!-- Keep one the following domains to track -->
                    <!-- <input type="hidden" name="domainAccountId" value="LAS-274365-01" tag="http://www.venuelife.ca"> -->

                    <!-- Keep one the following rotations (or default one will be used) -->
                    <!-- <input type="hidden" name="registrationPageIdentifier" value="Online" tag="Online"> -->

                    <!-- Keep one the following ratings (default is rating N) -->
                    <!-- <input type="hidden" name="RatingID" value="188942" tag="A"> -->
                    <!-- <input type="hidden" name="RatingID" value="188943" tag="B"> -->
                    <!-- <input type="hidden" name="RatingID" value="188944" tag="C"> -->
                    <!-- <input type="hidden" name="RatingID" value="188945" tag="D"> -->
                    <input type="hidden" name="RatingID" value="188946" tag="N">
                    <!-- <input type="hidden" name="RatingID" value="188947" tag="Not Interested"> -->
                    <!-- <input type="hidden" name="RatingID" value="188948" tag="Not Qualified"> -->
                    <!-- <input type="hidden" name="RatingID" value="188949" tag="Purchaser"> -->
                    <!-- <input type="hidden" name="RatingID" value="188950" tag="Realtor"> -->

                    <!-- Keep one the following source types (default: Online Registration) -->
                    <!-- <input type="hidden" name="SourceTypeId" value="55783" tag="Call In"> -->
                    <!-- <input type="hidden" name="SourceTypeId" value="55784" tag="Call Out"> -->
                    <input type="hidden" name="SourceTypeId" value="55785" tag="Online Registration">
                    <!-- <input type="hidden" name="SourceTypeId" value="55786" tag="Outside Realtor Transfer"> -->
                    <!-- <input type="hidden" name="SourceTypeId" value="55787" tag="Walk In"> -->

                    <div class="row">
                        <button type="submit" id="contact-submit" class="btn btn-default btn-big">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-152 174.4 494.4 252.6">
                                <title>Layer 1</title>
                                <path d="M203.4 183.9c3 .8-1.7 4.7.8 5-1.5 2.5-2.6-2.4-2.5-4.2 1 .2 1.5-.1 1.7-.8zm84.9 6.7h-5c.4-2.7 4.5-3.2 5 0zm-218.1-.9c-1 3.8-5.7 3.7-10 4.2 2.1-2.6 5.6-3.9 10-4.2zm-16.6 8.4c.4 4-4 3.2-5.8 5-16.2 12.1-27.6 29-40 44.9 10.9-21 23.5-40.3 45.8-49.9zm36.6 13.3c-6.4 6.7-12.3 13.7-20 19.1 4.8-8.3 10.6-15.5 20-19.1zM-112 225.5c-.6.8-1.5 1.3-2.5 1.7-6.5-7.5-21.1-10.9-29.1-10v-3.3c12.6-1.6 26.1 3.2 31.6 11.6zM87.7 228c-.9-3.1 3.3-8.3 6.7-10-4.1-3.1-9 6.3-12.5 9.2-1.1-5.9 8.2-8.5 10-14.1 3.9-1.7 8.5 4.6 3.3 4.2.4 1 1.1 1.7 2.5 1.7-.7 2.4-1.9 4.2-4.2 5 1.9-3.5-5.1 2.1-5.8 4zm-237.2-7.5c3.6 9.4 0 26.2.8 36.6-5.7-7.2-1.3-24-.8-34.1 0-.5-1.3-1.5 0-2.5zm17.5 5c5.5 2.9 14.6 2.1 18.3 6.7-2.7.5-3.9-.6-5.8-.8 2.6 6.6 11 7.3 12.5 15-3.5 1.3-9.7-1.2-13.3-2.5.6 3.6 9.4 6.2 11.7 10.8-.5 3.5-6.5 2.7-9.2 1.7-.1 5.1 5.3 4.7 5.8 9.2-4-2.3-9.2-3.5-9.2-10 3.1-.4 3.8-.6 8.3-.8-2.8-6.7-15.1-8.1-15-15 4.8 2.2 10.9 2.9 15.8 5-1.8-7.1-11.5-13.4-20.8-15.8-.1-1.6 1.1-1.9.9-3.5zm311.3 11.7c4.8 2-1.9 5.5-2.5 7.5-2.7-2.7 2.7-4.4 2.5-7.5zm-77.4 16.6c1.3 2.3 3-4.5 5-1.7.2 3.6-3 3.6-4.2 5.8 5.1 3.2 4.8-6.3 9.2-6.7-.2 4.2-3.1 5.8-4.2 9.2 5 1.7 3.9-9.3 9.2-10-1.9 4.8-3.6 9.8-6.7 13.3-13.6-.5-21.9-6.4-27.5-15 6 4.2 11.9 8 17.5 8.3.6-3.7-4.7-8.6-7.5-11.7 2.9-3.4 11.4 4.3 9.2 8.5zm-240.6-9.1c5.6 7.1 8.8 21.1 11.7 28.3-4.2-1.7-4.1-7.6-6.7-10.8-1.7 1.4-2.9 2.3-4.2 0-.8-2.5 1.9-1.5 3.3-1.7-1.9-4.7-5.2-8-4.1-15.8z" />
                                <path d="M-144.5 246.3c5.8 15.3 12.5 29.7 20 43.3-8.3-12.8-18.7-23.4-20-43.3zm219.7 9.2c3.4-1.7 6.5 4 10 5-1-2.6-7-7.4-10-10.8 4.6-2.6 10.3 7.2 13.3 11.7 4.5.8 6.9 3.7 10.8 5-8.1 2.9-19.5-4.8-24.1-10.9zm76.6-7.5c-1.6 5.3-3.7 10.2-8.3 12.5 2.1-4.8 3.4-10.4 8.3-12.5zm-30 .8c1.8 3.6-4.7 16.6-10 15.8 3.8-4.8 6.9-10.3 10-15.8z" />
                                <path d="M126 248.8h3.3c-2.8 7-4.2 15.3-12.5 16.6 2.8-5.7 8.3-8.9 9.2-16.6zm20 0c-.1 3.9-3 11.8-9.2 12.5 2.2-5 5.2-9.3 9.2-12.5zm80.7.9c2.1.7 5.8-.2 5.8 2.5-2-.7-5.7.2-5.8-2.5zm-87.4.8c-1.9 4.8-2.8 10.6-8.3 11.7 1.4-5.3 3.8-9.6 8.3-11.7zm-8.3.8c1.3-.2 1.4.8 2.5.8-3 2.8-2.2 9.4-7.5 10 .4-4.8 3.8-6.7 5-10.8zm59.9 2.5c-3.1 3.9-7.2 6.7-12.5 8.3 2.8-4.1 7.4-6.4 12.5-8.3zm5 2.5c-3.4 2.6-7.6 8.2-12.5 6.7 3.4-2.6 8.2-7.7 12.5-6.7zm-7.5 9.2c1.2-4.1 5.3-5.2 9.2-6.7-2.2 3.2-4.2 6.4-9.2 6.7zm-339.6-5c4.8 1 0 11.6 5 12.5-2 1.5-3.4-2.2-5.8.8.1-5.6-.2-10.7.8-13.3zm358.8 7.5c1.2.9 6.4-1.3 8.3-2.5 3.2.8-2.8 3.1 0 3.3-8.9 2.7-15.6-.4-24.1-1.7 2.7-3.2 6.5-5.2 11.7-5.8-1.8 2.6-6.2 2.7-7.5 5.8 5.1-1 7.4-4.8 12.5-5.8-.2 3.4-4.4 2.8-5 5.8 3.4-.2 4.9-2.3 7.5-3.3 2.1 1.5-2.6 3-3.4 4.2zm15.8-.8c0 3.1-3.7 2.4-6.7 2.5.5-2.6 4.8-1.3 6.7-2.5zm-46.6 9.9c-5.7 2.3-9.3-4.5-16.6-3.3 3-4.9 12.8 1 16.6 3.3zM62.7 273h2.5c-2.4 11-4.8 21.8-11.7 28.3 2.8-9.8 8.7-16.4 9.2-28.3zm5.9.8c1 4.9-3 21.7-10 25.8 3.8-8.1 6.3-17.6 10-25.8zm103.2 5c-3.9 4.4-8.6-3.2-15-1.7 4-4.9 10 2.1 15 1.7zM-152 278c2-.3 1.8 1.5 4.2.8 1 7.2 2.1 17.8 2.5 25-5.3-4.8-3.7-16.9-6.7-25.8zm27.5 1.6c2.3.2 3.5 1.4 3.3 4.2-2.6.1-2.9-2.1-3.3-4.2zm175.6.9c1.1 5.1-2.4 19.8-9.2 22.5 2.6-8 7.2-14 9.2-22.5zm-11.7 6.6c-8.2 14.9-12 34-25.8 43.3 8.5-13.7 16.9-27.5 23.3-43.3h2.5zm-59.1 2.5c1.2 5.6-6 10.1-8.3 15-.5-5 6.2-9.9 8.3-15zm49.1 1.7h2.5c-7.8 13.9-11.4 31.8-24.1 40.8-.9-2.5 7.7-12 10.8-16.6 2.9-8.8 8.2-15.2 10.8-24.2zm-5.8.8c1-.1 1.5.2 1.7.8-4 7.6-6.6 17.4-10.8 25-3.5 6.3-8.3 14.1-15.8 15.8C9 321.1 15.5 308 23.6 292.1zm129 1.7c-.9 3-2.9 4.9-5.8 5.8.3-3.6 2.2-5.5 5.8-5.8zm-136.5 1.7c1 6.1-6.5 17.8-11.7 23.3 3.2-8.5 7.9-15.5 11.7-23.3zm-5 1.6c-1.1 6.7-6.3 16.5-12.5 20.8 3.7-7.4 8.1-14.2 12.5-20.8zm-27.4 5c2.3 1.3-1.3 3.6-1.7 5-2.3-1.2 1.3-3.6 1.7-5zm15 16.7c-2.1 5.7-7.3 15.4-15.8 17.5 5.2-5.9 8.8-13.4 15.8-17.5z" />
                                <path d="M2 319.6h2.5c-2.7 7.6-6 14.5-15 15.8 3.7-5.7 10-8.9 12.5-15.8zm-89.1 5c1.3-.2 1.4.8 2.5.8 3.3 15.8 6 32.3 12.5 44.9-10.1-6.8-10.3-33.2-15-45.7zm223.1 4.1c-3.4 4.9-3 13.6-8.3 16.6.6-4.4 2.6-14.4 8.3-16.6zm-24.2 14.2c-1.9.3-3.2-.1-4.2-.8.9-1.9 4.3-2.1 4.2.8zm32.5 5.8c-.1-2.6 2.1-2.9 4.2-3.3 1.2 1.4 1.9 7.5-1.7 6.7.2-2.2-.3-3.6-2.5-3.4zm-14.1-.8c1.3-.2 1.4.8 2.5.8-1.9 3.4-3.2 7.3-7.5 8.3.9-3.7 3.8-5.6 5-9.1zm-72.5 4.2c.7.4 1.4.9 2.5.8-4.8 4.6-7.1 11.8-14.1 14.1 2.1-6.8 8-9.7 11.6-14.9zm5 .8c3.1 2.1-2.8 7.7-6.7 8.3 1.2-3.8 4.8-5.3 6.7-8.3zm-16.6.8c0 3.9-2.4 5.4-4.2 7.5-3.2-1.5.2-8 4.2-7.5zm7.5 0c-2.6 3.2-4.4 7.3-8.3 9.2.8-5 2.7-8.9 8.3-9.2zm-192.3.8h2.5c-.4 5.7 1.5 9.1.8 15-2.2-3.8-2.3-9.8-3.3-15zm182.3 14.2c-.4 5.4-2.7 16.2-10.8 16.6 3.9-5.2 7.2-11.1 10.8-16.6zm87.4 0c-.9 2.7-2.4 4.8-3.3 7.5-3.2-1.2.3-7.2 3.3-7.5zm-152.3.8c-.1 4.4-7.2 8.9-10.8 12.5.7-3.7 7.1-8.9 10.8-12.5z" />
                                <path d="M133.5 369.5c1 4.4-3.9 9.9-5.8 14.2 6.2-2.4 5-12.2 11.7-14.2-4.1 7.5-6.7 16.6-13.3 21.6.4-2.4 2.9-2.6 2.5-5.8-3.7.5-2.5 5.8-6.7 5.8 1.5-3.8 4.5-6.1 5-10.8-4.6 2.1-3.6 9.7-9.2 10.8 2.2-5 4.9-9.5 7.5-14.2 4.4 4.2 5.3-4.9 8.3-7.4zm-97.4 1.7h3.3c-3.1 5.1-5.4 15-11.7 14.2 1.9-5.8 8.3-6.8 8.4-14.2zm34.1.8c-.5 5.9-.7 12.1-5.8 13.3 1.6-4.8 1.3-11.4 5.8-13.3zm72.4 0c-1.4 3.3-3.4 6.1-5 9.2-2.9-1.8 1.5-8.5 5-9.2zm-215.5.9c1-.1 1.5.2 1.7.8 2.8 5 5.3 10.8 8.3 15.8 2.9 4.8 8.2 8.4 8.3 13.3-9.1-7-13.4-18.7-18.3-29.9zm-5 1.6c2.4-1.2 4.1 5.3 5.8 7.5-5 .6-3-5.9-5.8-7.5zm47.4-.8c-1.4 4.2-3.6 7.4-8.3 8.3 1.8-3.7 4.6-6.5 8.3-8.3zm93.2 0c3.3 2.2.6 10.5-3.3 9.2 1.4-2.8 2.1-6.3 3.3-9.2zm-15.8 1.7c1.4 2.8-1.2 8.8-5 9.2.8-4.1 3.3-6.2 5-9.2zm10.8 0c1.1 2 .1 8.9-4.2 8.3.6-3.7 3.1-5.3 4.2-8.3zm-43.2.8c1.5 3.8-7 11.8-13.3 13.3 3.3-5.5 9.7-8 13.3-13.3zm-94.1 1.7c2.6-.1 2.9 2.1 3.3 4.2-2.6 0-2.8-2.2-3.3-4.2zm75.8.8c1.1 2.8-2.7 7.8-7.5 7.5 1.7-3.2 5.1-4.8 7.5-7.5zm104.8.8c3.8.1 6 1.7 9.2 2.5-2.5 1.9-8.8.5-9.2-2.5zm44.1.8c-1.2 1.6-4.4 3.7-5.8 2.5.4-.7.9-1.4.8-2.5 2.7.5 3.3-1.8 5 0zm-271.3 1.7c1.5 2.1 3.8 3.4 4.2 6.7-2.5-1.2-4.3-2.9-4.2-6.7zm257.2 9.2c.5-4 3-5.9 4.2-9.2 2.8 1.8-.9 3.8 5 2.5-.7 4.3-4.3 9.3-9.2 6.7zm-113.2-7.5c-6.2 5.2-11.4 12.9-19.1 14.1 5.9-4.6 11.8-13.6 19.1-14.1zm35 .8c1.3-.2 1.4.8 2.5.8.7 2.7-2.2 1.7-2.5 3.3-2-.1-.5-3.6 0-4.1zm-10 .8c-2.4 2.1-.9 7.9-6.7 6.7 1-3.5 3.4-5.5 6.7-6.7zm5 0c-.5 2-1.1 3.9-3.3 4.2.4-2 1-3.9 3.3-4.2zm95.7.9c-.9 2.1-1.6 4.5-3.3 5.8h-2.5c1.2-2.7 2.3-5.4 5.8-5.8zm-109.1.8c-1.9 4.3-3.7 8.5-10 8.3 3.6-2.5 3.8-8.4 10-8.3zm29.2 0c3.8.4.8 6.3-2.5 5 0-2.5 2.1-2.8 2.5-5zm-184.8 2.5c4.4 4.5 10.1 7.6 12.5 14.1-5.8-3-9.2-8.5-12.5-14.1zm268 0c-1.2 1.3-1.8 3.2-2.5 5-1.5.1-1.8-1-3.3-.8 1.2-2.1 2.8-3.8 5.8-4.2zm2.5 2.5v2.5c-1.5-.2-4.2.8-3.3-1.7 1.5.1 1.7-1 3.3-.8zm-255.5 19.1c6.1 3.9 11.5 8.5 16.6 13.3-6.6 0-13.4-7.1-16.6-13.3zm-.9 5.9c3.5-.4 8.4 4.9 10.8 8.3-3.6.5-5.4-.7-6.7-2.5-.6.7.3 3.7-3.3 3.3-2.5-3.6 2.8-5.9-.8-9.1z" />
                                <path d="M43.6 196.4c-18.4 8.1-38.8 34.3-47.4 53.3-1.5 3.4-.8 8.5-5.8 9.2 9.7-23.3 22-44 40.8-58.3C3.3 214.1-4.1 248-20.5 273c9-26.8 21.4-50.2 39.1-68.2-33.3 23-40.9 79-64.9 114 11.1-5.8 14.6-26.4 25.8-35C-28 296-33.8 309.9-43 320.4c4.3.1 7.3-8.3 10.8-11.7-.5 5.9-6.9 13.1-10 19.1 11.8-9 16.9-24.7 27.5-35-6.2 15.2-16.6 26.1-25 39.1 6.7-6.1 12.4-13.1 17.5-20.8-.8 8-9 16-14.1 22.5 11-6.7 19.7-23.1 27.5-35.8 2.9 2.2-2.2 9-5 10.8-3.3 10.9-11.1 17.2-16.6 25.8 6.2-1 6.7-7.7 11.7-10-.2-2.4 1.9-2.6 1.7-5 7.1-5.1 8-16.4 15.8-20.8-7.5 14.8-14.9 25.5-24.1 37.5H-32c18.9 5.1 39.5-.2 54.1-7.5-.5-.8-1.9-.9-1.7-2.5 5.3-6.3 11.8-13.2 15.8-21.6 3.3-7 3.1-17.1 10.8-20.8-6.3 13.6-11.2 28.7-20 40 7.7-4.2 9.8-14.1 16.6-19.1-3 10.6-10.5 16.7-15.8 25 7.4-4.6 13.9-17.2 20-25.8 2.5 3.4-3.6 7.9-5.8 11.7-3 5-4.6 11.1-9.2 13.3 8.6-3.6 14.2-17.4 20-26.6 1.3 7.5-8.5 18-13.3 25 9.5-2.7 11.9-19.7 20-26.6 2.4 1.9-1.4 4.7-1.7 6.7 3.4-.7 2.3-6.1 5.8-6.7 2.4 2.6-2 3.5-1.7 6.7 5.2-4.5 8.6-10.8 10-19.1-4.3 2.4-2.6 10.7-8.3 11.7 3.2-8.1 5.8-17 10-24.1.7 4.3-1.8 5.5-1.7 9.2 3.9-1.4 2-8.6 6.7-9.2 2.1 2.2-1.8 3.1-1.7 5.8 1.4 1.9 3.1-2.9 3.3-5 4.4 1.9 2.2-2 6.7-1.7-.2 3.7-2.1 5.6-4.2 7.5 2 .6 5.7-4.2 6.7-7.5-6-3.2-18.3 0-20-7.5 4.5 2.2 11.9 3.9 15.8 4.2-4.1-3.9-13.3-2.8-15.8-8.3 10.3 2.3 28.3 7.8 38.3 8.3-1.9-3-5.5-1-7.5-2.5 2.7-3.2 6 0 9.2 0 5.2-.1 10.1-3.4 15.8-4.2 9.6-1.2 20.9 1.3 30-5-1.9-1.6-3.6-1.4-6.7.8-.1-4.4 4.8-10.6 9.2-13.3-1.1 3.9-3.8 6.2-5 10 1.8-.4 7.6-4.1 6.7-10 1.7-.4 2.2.6 3.3.8 1.4-4.2-.9-12 3.3-13.3 1 4.8 1 10.7 1.7 15.8 2.8-2.2 1.3-8.7 4.2-10.8 2.2-.3-.8 4.7 2.5 3.3-2.6 1.5-2.5 2.4-1.7 5.8 1.8 1.7 6-5.6 8.3-3.3-1 4-6.6 3.4-8.3 6.7 2.8 2.2 6.6-5.3 9.2-3.3-1.6 4.5-6.6 5.6-9.2 9.2 3.9 1.2 9-7.5 13.3-5.8-2.3 4-6.1 6.6-10.8 8.3.7 1.5 3.4 1.1 2.5 4.2 14.1 3 36.6 12.4 50.8 2.5 11.6 1.9 23.4-6.8 20-20 .2 20.4-22.9 20.4-38.3 11.7-2.9-2.1-3-7-7.5-7.5-.9-8.3 3.6-14.2 10-17.5 7.6 2.7 18.5 2 26.6 4.2-4.9-5.3-17.4-5.7-25-5.8-4.1-4.2-10.4-6.2-15.8-9.2-32.9-7.5-80.6-6.5-114.9-14.1-2.5.7 1.9 1.2.8 3.3-9.1 4.2-13.3 13.4-20.8 19.1 5-8.3 10.2-16.4 19.1-20.8-1.8-3.7-5.9-.6-5 1.7-8.4 4.6-13.1 12.9-20.8 18.3 3.9-8.3 10.1-14.3 17.5-19.1-.8-1.5-2.6-1.8-5-1.7.4 1.3 2.6.7 1.7 3.3-11.2 4.9-15.9 16.3-25 23.3 5.6-9.4 12.9-17.1 20.8-24.1-11.3 4-19.2 18.5-29.1 26.6 4.5-9.9 12.1-16.8 19.1-24.1-12.3 5.4-16.7 18.8-27.5 25.8 5.6-9.7 13.4-17.1 20.8-25-10.7 6.1-17.9 16.1-25.7 25.5 1.7-8.6 12.1-15.7 17.5-23.3-11.5 5.7-17.3 17.1-25.8 25.8 8.9-18.6 24.1-35.2 41.6-42.4-3.6 5-10.7 6.5-14.1 11.7 7.9-2.4 17.2-10.6 27.5-13.3-1.4 3.8-6.5 4.1-8.3 7.5 12.8-3.2 36.4-3.5 49.9 1.7 2.5-.5-.8-1.8 0-3.3 2.5-.6 2.4 1.4 5 .8-.5-2.8-2.8-3.9-2.5-7.5 3.6 1.1 5.5 4 9.2 5-.1-4.7-4.1-5.3-4.2-10 6.7 2.5 8.2 10.2 14.1 13.3 4.3-1.9.1-4.4 1.7-7.5 3.6-.3 3.5 3.2 5 5 2.1-2.3.8-7.3 5-9.2 2.2 1.1 2.6 1.7 1.7 4.2 5.7.7 4 2 8.3 1.7-.2 3.4-2.2 5.1-3.3 7.5 3.6-.8 3.9-5 6.7-6.7 3.5 1.2.3 5.9-.8 7.5 4.2 1.7 3.5-8.7 9.2-8.3.7 3.5-1.6 3.9-1.7 6.7 2.5-2 3.1-5.8 7.5-5.8.4 3.8-1.7 5-2.5 7.5 3.5-1.5 4.2-5.8 6.7-8.3 3.1-.6 2.1 2.9 3.3 4.2 2.9-1 1.8-5.9 5.8-5.8-.5 4.8-3.8 6.7-4.2 11.7 4.5-3.3 4.4-11.2 10-13.3 1 3.2-3.1 7.8-4.2 11.7 7.6-2.9 5.4-15.7 14.1-17.5-3.5 7.9-6.6 16.2-12.5 21.6 9.2-4.1 11.5-15.2 16.6-23.3 4.7 3.1 2.8 13.5 1.7 18.3 4.6.8 1.9-5.9 5-6.7 3.1-.8 1.5 4.4.8 5 1.1 1.8 2.7-2.8 5.8-1.7.1 1.2.1 2.4-.8 2.5 1.4 2.1 2.2-3.2 5-2.5.1 2.1.1 4-.8 5 1.6 2.1 2.3-3.8 5.8-2.5.6 2.5-1.4 2.4-.8 5 3.3-.6 2-5.8 5.8-5.8.3 3.1-.9 4.6-1.7 6.7 5.8.5 2.4-8.2 9.2-6.7-.1 2.4-1.5 3.5-1.7 5.8 2.9 1.8 2.6-6.8 7.5-5.8.7 2.9-1.2 3.2-.8 5.8 3.2 2.3 1.8-7.1 6.7-5.8 1.5 1.6-.2 4.7-.8 6.7 3.9-1.1 2.9-7.1 7.5-7.5-.1 2.6-1.9 3.7-1.7 6.7 3.1-1.4 2.5-6.4 5.8-7.5 1.5 2.1-1.5 5.2-1.7 8.3 3.3 1.6 2.6-7.9 7.5-7.5.4 3.2-1.5 4.1-.8 7.5 6.1-5.5 2.5-12.5 5.8-19.1 4.5-.9-.2 2.8 4.2 2.5-4.8 6.5 2.9 10.2.8 15.8 1.5 1.7 3.3-3.3 5-4.2 3.2.8-.3 4.7-.8 6.7 3.2-1 3.3-5 7.5-5 .7 3.5-1.6 3.9-1.7 6.7 3.8.4 2.2-4.4 5.8-4.2 1.8 1.8.2 3.7-.8 5 3-.3 4-2.7 5.8-4.2 2.5-.8 1.5 1.9 1.7 3.3 2.6.4 1.9-2.6 5-1.7 1.6 1.6.1 2.1 0 4.2 4.6-4.2 10.4 0 15.8-3.3 1.8-.4 1.6 1.2 1.7 2.5 2.5-.5 3.4-5.4 5.8-3.3-.4.7-.9 1.4-.8 2.5 3.8-2 3.3-8.3 9.2-8.3.9-5.4-2.1-6.7-2.5-10.8 14.3 10.2-1.1 22.4-10.8 27.5-29 2.3-53.4-5.9-84.9-.8-.7-2.7 2.7-1.1 2.5-3.3-1.5-2.5-2.6 2.4-5.8.8-3.7.1 1.1-2.4.8-4.2-2.2 2-3.6 4.7-7.5 5-2-2 1.2-1.5.8-4.2-2.7 1.2-2.6 5.1-7.5 4.2-.4-3.1 1.6-3.9 2.5-5.8-3.5.9-3.5 5.4-7.5 5.8-1.6-1.6.5-1.5 0-4.2-3.2 1.9-3.2 5-8.3 3.3 6.6 3.3 19.4 1.3 27.5.8-3.7 4.2-7.9 1-15 3.3 8 4.7 16.8 2.6 24.1 8.3 9.6 7.4 6.4 24.4-1.7 32.5-8.3 2.8-16.7 5.5-25.8 7.5-1.5 14.3-10.9 20.7-19.1 28.3 7.3 6.2 3.7 18.6 4.2 29.1-3.5 1.5-4 6-9.2 5.8-5.6 6.1-1.7 17-11.7 19.1 1.2 4.9 3.1 9.1 5 13.3-2.2 15.3-12.2 22.8-18.3 34.1-21.4 12.1-52.4 19.3-83.2 18.3-35-1.1-66.1 2.2-95.7-8.3-12.5-.9-22.2-4.4-31.6-8.3-5.7 7.2-11.1 26.6-25 17.5-2.9-.5-2 2.9-5 2.5 2.4-6.2-5.5-8-8.3-11.7 1.1 3.4 3.9 5 5.8 7.5-2.7 1.3-9.2 2.1-13.3 2.5-6.1-7.5-15-12.2-20.8-20 9.8 4.9 15.6 13.8 25.8 18.3-7.1-10.9-21.4-14.6-25.8-28.3 6.4 5.2 11.3 12 17.5 17.5-1-8.1-12.2-16.9-17.5-20.8 6.7-3 11.6 10 17.5 13.3.5-7.8-13.3-14.7-16.6-24.1 6.8 2.7 8.8 10.1 15 13.3 1.4-1.2-1-5.6-1.7-7.5-14-11.5-26.1-24.9-36.6-40-4.8 2.6.9 8.5 3.3 11.7 5.5 7.3 15 15.4 18.3 22.5-9.2-6.1-13.6-16.9-23.3-22.5 3.7 10.8 16.3 19.7 23.3 30-8.8-2.9-14.2-16.3-22.5-22.5 1.2 8.8 11.3 15.9 15.8 24.1 3.1.9 6.3 2.9 5 5.8-7.8-6.9-15.4-14-20-24.1-1.1 16.7 8.8 26 20 34.1.5 4-1.1 5.9 0 8.3-1.7-.2-1.6-2.3-3.3-2.5-2.4 2.3 3 5.4 0 8.3-2.8-1.8-2-2-5-3.3-3.4-12.6-21.9-27.3-18.3-44.9 4.3-6.6 1.4-15.9.8-25-1.3-20.8-.7-42.4-5.8-60.8 2 1.2 1.5 1.8 4.2.8 7.2 16.8 16.7 30.9 29.1 44.1-1.5-12.6-13.4-22.2-20.8-31.6-4.6-12-12.8-22-15.8-33.3 12.4 16.7 19.6 38.6 35 52.4-.8-11.1-9.5-14.4-12.5-23.3 4.5 2.1 6.1 7.2 10 10-.4-7.5-7.8-11.4-10-20.8 4.9 2.6 6.1 8.9 10.8 11.7-.6-12.5-13.1-20.2-16.6-32.5 6.5 1.3 7.4 15.3 14.1 19.1 1.9-.9-1.2-3.2 1.7-4.2 4.1 3.4 1.2 13.2 1.7 18.3.1 1.3 1.4 2.1 1.7 3.3 8.2 38.7 14.6 80.5 27.5 114.9-.9-39.6-13.2-79.2-19.1-114.9 4.4 2.3 3.2 9.1 4.2 13.3 4.6 21.2 12 46.8 14.1 70.7 4.4 4 1.6 10.4 5.8 15-.7.9-2.3-.5-1.7 3.3.6 2.8 5.7.9 6.7 3.3 3.1-.5-.9-3.5 3.3-3.3v3.3c2-.5 3.7-.5 5-3.3-5.7-4.8-14.4-11.1-14.1-19.1 4.1 2.8 4.5 9.4 9.2 11.7-.6-4.8-5.9-8.9-5-12.5 2.9 1.8 4.4 6 6.7 10 2.8 4.8 7.4 9 5 11.7 3.1.4 5.5.9 10.8.8-16.3-19.7-26-46.1-32.5-75.7 3.8.6 2 4 2.5 5.8 7 24.3 16.4 48.2 31.6 66.6-1.5-5.2-7.8-12.7-10.8-19.1 3-.7 4 1.9 5.8 0-.6-3-3.3.2-6.7-.8-20.9-35.9-25.3-91.6-21.6-133.2-4.6 2.3-8.5.3-13.3 2.5-7.5 22.1 6 46.3 8.3 69.1-3.2-3.5-4.4-9.8-5.8-15.8-2.5-10.8-5.3-21.6-5.8-35.8-3.7 6.2.3 21.2 0 30-3.4.2-4.2-5.2-2.5-7.5-6.9-18.3-.9-41.4 6.7-59.1-1.4-2.1-2.2 3.2-5 2.5 1.9-2.5-4.7-2.1-7.5-2.5 1.7-4.9 12.4-.9 14.1-5.8-4.2-3.6-12.3 3.7-15-3.3-1 .1-.7 1.5-.8 2.5-2 .3-1.8-1.5-4.2-.8v-4.2c17.9 1.1 43-2.5 45.8 20.8 30.2-22.7 69.4-35.9 110.7-49.1 15.4-4.9 29.6-13 47.4-15 24-2.7 48.4-1.1 72.4-1.7 14.6-.3 29.5-1.8 44.9-.8 2.3.4 3.5 2 5.8 2.5 35-1.9 67.2 7.4 99.9 10 4.9 5.1 16.3 1.5 20 9.2-20.1-7.5-44.7-10.7-65.8-7.5-17.6-7.9-40.9-6.5-60.8-9.2-.8.5-.9 1.9-2.5 1.7-11.2-5.4-32.7 1.3-48.3-4.2-11.1 4.5-22.2 2.5-33.3 1.7-43.5-3.5-80.6 14-102.4 40.8-6.5 11-14 20.9-21.6 30.8 8.6-23.8 24.8-40.2 43-54.4zm153.1 18.3c4.7-3.6 11.2-9.9 9.2-17.5-2.3 6.6-5.6 12.2-9.2 17.5zm-19.9-4.1c2.9.1 2.6-2.9 4.2-4.2-.5 0-1-.1-.8-.8h-1.7c.3 2.6-1.3 3.2-1.7 5zm-194 38.2C-31.8 270-37.5 300.1-53 320.4c10.4-6.5 13.1-20.8 20.8-30 11.1-30.2 20.4-62.3 42.4-81.6-.1-1 .1-2.4-.8-2.5-12 11-22.1 24-26.6 42.5zm72.4-38.2c2.5-.3 5.1-.5 5.8-2.5-2.7-.1-4.3 1.1-5.8 2.5zm146.5 6.6c.7-1.2 1.2-2.6 3.3-2.5-2.1-2.4 1.5-4.8 0-5.8-.8 2.5-4.9 6.1-3.3 8.3zM-19.7 238c5.1-2 7.5-9.3 10.8-14.2 3.4-5.1 9.2-10.3 8.3-14.1-6.6 9.1-16.2 15.4-19.1 28.3zm-52.4 101.6c2.3-.6-.6 3.9 1.7 3.3 3.1-7.4 11.8-13.7 11.7-21.6-4.5 6.1-7.8 13.3-13.4 18.3zm63.3-125.7c-28.9 31.8-36.3 85.2-61.6 120.7 11.3-11.7 16.6-29.5 26.6-42.4-3 11.2-9.6 18.7-14.1 28.3 16-17 25.7-46.2 35.8-69.1 1.1-2.6 3.5-9.9 2.5-11.7-10.8 14.1-13.7 36.2-24.1 50.8 8.7-25.2 20-54.8 34.9-76.6zm-61.6 112.3c-2 3.1-5.6.8-3.3 5.8 17.7-20.3 28.6-54.6 40-84.1 5.6-10 13.2-22.3 16.6-31.6-24.8 29.7-32.7 76.2-53.3 109.9zm-3.4-3.3c19-19.8 22.9-54.8 37.5-79.1.6-6.6 11.5-18.8 10.8-23.3-23 27-30 73.4-48.3 102.4zm30.8-88.2c.2-1.8 3.8-4.5 1.7-5.8-15.7 20.9-26.6 51-33.3 78.2 4.5-5 6.8-11.3 9.2-18.3 1.1-3.4.6-7.8 1.7-10.8 2.4-7.2 7-14.4 10-21.6 3-7.4 5.4-15.8 10.8-20-11.7 26.8-21 51.4-30.8 79.1 5-.6 3.6-7.5 7.5-9.2 8.7-28.7 18.9-56 33.3-79.1-.1-1-.2-2-1.7-1.7-3.3 2.6-3.9 7.8-8.4 9.2zm-33.3-1.7c-.3-3.1-3.5-3.1-5-5 .3 3.1 2.9 3.8 5 5zm0 64.9c.3 1.9-2.4.9-2.5 2.5 2.1 2.2-1.7 7.2 1.7 9.2 4.5-28.1 17.3-52.5 29.1-75.7-13.6 17.1-22.6 38.9-28.3 64zm4.2-54.1c.3-4.8-1.2-7.7-4.2-9.2 2.4 2.2.3 8.7 4.2 9.2zm311.3-4.1c-5.3-.6-16.4-3.9-18.3-2.5 6.1.3 13 4.2 18.3 2.5zm-318.8 34.1c2.4 1.3-1.3 4.3.8 5 3-2.8 2.3-9.4 6.7-10.8-1.5 7.6-8.3 18.1-5.8 24.1 4.6-20.1 16.4-37.4 21.6-54.1-6.9 7.9-10 23.8-17.5 28.3 3.4-7.5 9.3-16.8 10.8-23.3-2.4 2-3 5.9-6.7 6.7.4-1.2 1.2-2.1 1.7-3.3-3.6 1.9-11.1 4.5-9.2 9.2.8-2.2 1.4-4.7 4.2-5-2.2 10-7 10.8-6.7 19.1 4.2-4.4 4.3-12.9 9.2-16.6-2.3 6.9-5.7 20.1-9.1 20.7zm323-28.3c-11.2-2.8-28.4-13.2-35-1.7 2.4 1 5.5 1.2 6.7 3.3-3.7.4-4.1-1.1-6.7 0-6.9.8 4 1 3.3.8 6.8 1.9 20.4 7.6 29.1 8.3.1-3.7-6.3-1-6.7-4.2 3.7-.7 5 1.1 8.3.8 1.1-2.3 2.1-4.8 1-7.3zM157.6 258c3.6-.3 8.7 1 10-1.7-2-.5-6 1-6.7-.8 1.9.5 4.4-4.4.8-4.2-1 2.6-2.7 4.4-4.1 6.7zm81.6 0c-9.5-1-18.4-7.1-27.5-5.8 9.2 1.4 18.9 6.6 27.5 5.8zm-7.5 1.7c-2.7-.7-7.4-3.7-10-1.7 3.3.1 8 3 10 1.7zm-63.3 29.9c-3.4 1.4-3.8-4.4-5.8-1.7 1.7 1.4 3.9 2.2 4.2 5-2.5.8-2.6 4-5.8 4.2.2-1 3.4-3.1.8-4.2-1.3 3.2-5.7 7.6-9.2 5.8 2.7-2.6 5.5-5 8.3-7.5-10.8-11.6-33.9-15.9-51.6-10.8-7.1 4.3-16.7 22.4-6.7 30.8-6.5-8.7.3-26.3 9.2-26.6-.9 2.9-9.6 6.2-5 9.2 2.2-8.8 9.9-7.6 16.6-10.8 0 1.2-3.4 3.7 0 3.3 2.8-.5 3.9-2.8 7.5-2.5-.3 3.3-2.9-.9-2.5 2.5 2.9 1.8 7.5 3 12.5.8 0 2.4-4.7 2-2.5 4.2 3.2.2 4.5-4.3 7.5-1.7-.4 1.6-4.4 3.9-2.5 5 .1-1.6 3.7-3.9 5-1.7-3.4 1-4.3 4.6-7.5 5.8-7.5-3.6-20.3-1.9-28.3-5-1.5.3-5.1 2.5-2.5 4.2 9.8-.9 26.4-1.5 32.5 4.2.3-1 2.6-3.9 3.3-1.7.1 3.7-4.3 2.9-5 5.8 5.6.7 6.9-6.7 10.8-5-3.4 4.3-7.9 7.6-12.5 10.8 7.1 1.4 11.9-10.7 18.3-10-3.7 6-13.6 10.3-15.8 15 8.5-4 14.4-10.6 21.6-15.8.1-3 3.5-6.9 5.8-5-6.9 8.4-14.1 16.5-24.1 21.6 9.6.4 14.9-10.5 22.5-15-2.7 7.9-10.5 10.6-15.8 15.8 7.4 1.2 10.7-8.7 17.5-10.8-1.5 4.9-6.5 6.3-8.3 10.8 6.8-.3 8.6-3.6 13.3-5.8 16.9-7.9 40.6-16.5 40.8-38.3-12.4-1.2-22.5-1.2-31.6-4.2 2.4.4 6 2.1 4.2 4.2-4.4-1.7-10.9-5.8-15.8-4.2 3.4 1.4 10.8 3 10 5.8-5.1-2.7-11.7-3.8-16.6-6.7 3.8-2.2 8-3.3 12.5-.8-7.9-9.4-20.7 1.6-24.1 9.2 5.2 2.3 14.9 0 16.6 7.5-4.9-.9-8.5-3.2-11.7-5.8.6 7.5 8.7 2.8 9.9 10.1zm-81.5-4.1c.4-3.9 1.6-1.6 2.5 0 1.4-3.4 3.7-8.8 2.5-9.2-.2 4-9.3 6.1-5 9.2zm4.1 9.1c2.8-3.5 4.1-8.6 5-14.1-4.1 1.5-7.1 8.4-6.7 11.7.2-.7 2.3-3.6 2.5-1.7-.8.3-1.9 4.5-.8 4.1zm-10.8-8.3c1.4 4-2.8 6.6-.8 8.3 2.4-2.1 2.5-6.4 4.2-9.2-1.6-.1-1.9 1-3.4.9zm4.2 9.2c-3.7 1.3-5.5 8.9-5 11.7 2.1-3.5 4.8-6.3 7.5-9.2-3.9-4.3 1.1-9.4.8-11.7-.8 3.5-6 4.6-3.3 9.2zm-14.2 4.9c4.7-1.4 4.5-7.7 6.7-11.7h-2.5c-.8 4.6-3 7.7-4.2 11.7zm47.5-6.6c4.4-.8 12.4.5 18.3.8-1.6-5.3-9.7-4.2-15-5.8 2.8 4.2-3 1.8-3.3 5zm-248.9 15.8c-1-.2-1.7-1.3-1.7.8-.6 4.2 1.4 5.8.8 10 11.2 12.4 18.9 28.3 33.3 37.5-1-8.1-6.3-10.9-10.8-15.8-9-9.9-17.8-19.6-21.6-31.6 9 11.8 16.6 25.1 28.3 34.1 2.2-2.3-1.1-6.1-1.7-8.3-12.2-8.9-22.3-27.1-31.6-41.6.1 6.5 3.8 9.5 5 14.9zm264.7-9.2c-12.4.5-16.4-1.5-25.8-1.7 4.4 3.8 20 5 25.8 1.7zm-69.9 21.7c3.4-1.8 7.2-3.3 9.2-6.7-1-.1-2-.2-1.7-1.7 2.7-3.7 7.2-9 6.7-13.3-3.1-.9-2.4 2.1-5 1.7 1.8 3.4-.5 6.3-3.3 7.5-.1-2.1 3.7-4.5 1.7-5.8-1.2 2.1-5.4 1.2-5.8 4.2 2.5 4.2-5.2 9.8-4.2 12.5 4.4-1.4 3.9-7.7 9.2-8.3-1.7 3.4-6.6 7.9-6.8 9.9zm44.1-17.5c9.3.1 19.9 3.7 29.1.8-8.6-1-24.3-3.6-29.1-.8zm-33.3 10c.2-1.2 2-.8 3.3-.8-.1-5.1 7.7-6.7 6.7-10-1.7 4.7-10.9 6.3-10 10.8zm51.6-5c-1.5 1.8-6.1.5-7.5 2.5 6.7.1 13.7.4 16.6-3.3-8.4.7-23.5-3.7-28.3 0 7.7-.1 13.4 1 19.2.8zm-80.7 18.3c5.3-5 13.4-11.5 12.5-20-4.5 5.4-11.3 17-12.5 20zM146 358.7c.8-1.5 1.5-2.9 4.2-2.5-.2.5-2.1 3.2 0 3.3.8-1.7 1.4-3.6 3.3-4.2 2.5 1.2-1.8 4.7 1.7 4.2.5-2.6.2-5.9 3.3-5.8.3 1.9-.1 3.2-.8 4.2 3.8.7 1.9-4.2 5-4.2 2.4-.1-.7 5.1 1.7 5 2-1.9.7-7.1 3.3-8.3 3.3-.2.2 5.9 2.5 6.7 1.1-2.5 1.2-6 2.5-8.3 1.8.7.7 4.3 2.5 5 1.4-2.2-.2-7.4 3.3-7.5-.2 3.5 1.1 3.2 0 5.8 4.3.4 1-6.7 3.3-8.3 2.4 5.2 2.6-6.4 6.7-.8-.6-3.9.5-6.2 3.3-6.7.5 1.2 1.3 2 2.5 2.5 4.5-7.8 5.7-16.1 10-22.5-4.6-.4-3.3-6.7-6.7-8.3-13.8 8.7-24.6 17.5-40.8 16.6 0 1.4-.6 2.1-1.7 2.5-10.7-3.9-24.4-2-32.5 1.7.4 5.1-7.3 9.8-1.7 12.5-.1-6.1 3.7-13.2 5.8-10.8 1.1 1.2-5.3 6.3-2.5 10 1-2.4 1.2-5.5 3.3-6.7.7 2.7.2 10.5-5.8 9.2.5 1.5-2.4 3.5 0 4.2-.6-4.3 2.7-3.4 3.3-.8-3.8 2.9-4.1 9.2-10 10-.1-1 .1-2.4-.8-2.5-1.6 3.6-10.5 0-13.3 2.5 13.6-2.9 28.2 9.3 43.3 4.2-2.9-1.2-3.9.9-5.8-.8 0-1.2 3.5-2.6 0-2.5-1.5-2.5-1.2 3.7-4.2 2.5-.2-4.1 2.8-5 3.3-8.3-4.2.5-3.3 6.1-7.5 6.7 3.6-4.7 4.4-12.2 7.5-17.5-5.2.8-2.1 4.9-5 7.5 0-1.1-1.5-.8-1.7-1.7 2-5.2 3.8-14.8 10.8-17.5-1.4 5-3.2 8-3.3 12.5 5.2-.7 1.8-7.6 7.5-9.2-1.8 6-4.5 11.1-7.5 15.8 1.2.1 3.1-.3 3.3.8-2 1.1-1.6 4.5-2.5 6.7 3.2-.1 2.5-8.6 6.7-5 .2 2.4-1.9 2.6-1.7 5 1.5-.1 1.9.8 1.7 2.5-.3-.2-.8.5.2.6zm-93.2-32.5c4.7-4.7 8.5-10.4 10.8-17.5-5.7 3.3-8.9 13.4-10.8 17.5zm57.4-10.8c6.8 2 19.1 1.9 24.1-.8-6.9.5-17.6.1-24.1.8zm-32.5 10c4-2.1 7.1-5.1 10.8-7.5-6.4-.3-7.9 4.3-10.8 7.5zm12.5-5.8c0 3.9-8.4 3.8-5.8 7.5 1.1-1.7 2.7-2.8 5-3.3 3.1 3 10.9 1.3 15 3.3-.9-6.2-8.4-.9-12.5-4.2 4-2.2 9.1-1.9 15-.8.5-.5 1.9-3.9 0-4.2-2.8 3.9-9.4 2.2-16.7 1.7zm29.1 2.5c-3.2-1.8-8.1-1.6-10-.8 4.6.4 8.4 3.1 10 .8zm-187.2 21.6c5.3-5.8 13.1-13.6 13.3-21.6-3.9 7.8-10 13.3-13.3 21.6zm1.6 3.4c-1.5.8-4.9 1-2.5 3.3 7.2-3.3 15.9-16.9 17.5-25.8-5.2 7.3-9.9 15-15 22.5zm-44.9 12.4c-6.8-6.9-11.4-15.5-19.1-21.6 7.8 20.2 24.8 31.2 38.3 45.8-2.1-11.7-12.3-17.1-19.2-24.2zm2.5.9c6.9.9 9.1 10.9 14.1 10.8-2.3-2.6-3.6-3-2.5-7.5-13.1-10.5-22.2-25-34.1-36.6 3.8 14.8 16 21.1 22.5 33.3zm48.3-1.7c5.6-7.4 15.9-14.6 15.8-25-3 2.8-3.9 7.7-8.3 9.2-.8-6.9 9.5-10.6 5-15.8-4.2 9.9-14.3 18.4-15.8 28.3 4.4-3.4 5-10.5 10.8-12.5 1.3 5.4-9 10.4-7.5 15.8zm153.1-30c1.5.8 4.2.3 5 1.7-1.3 1.3-8.6-1.6-10.8.8 9.3 2 18.6 4.1 28.3 5.8-1-8.3-15.9-11-22.5-8.3zm25.8 4.2c.2-1.8 3.8-4.5 1.7-5.8-1.2 1.4-5.4 4.5-1.7 5.8zm-40.8 13.3c-3.4-3.5-.5-14.1-1.7-17.5-4 5-.8 9.2-1.7 15 2.8-.7 1.2 2.8 3.4 2.5zm-68.2 5c8.9-3.3 17.6-11.3 20-18.3-6.7 6.2-13.2 12.4-20 18.3zm74.9-15.8c7 1 13.6 2.5 20 4.2-6.4 3.4-15.8-4.6-20.8 0 10.3 1.1 20.3 2.4 27.5 6.7-9.4.2-17.5-5.6-24.1-3.3 7.1 2.9 17.2 2.7 24.1 5.8-4.6 2.7-11.7-3.1-16.6 0 2.6.5 6.3-.2 6.7 2.5-4.3-.1-11.9-4-14.1-.8 4.5-.4 11.8 4.5 17.5 2.5-.9-.2-2-1.4-.8-1.7 3-.3 5.4.1 7.5.8 1.7-2.4 1.7-6.6 5-7.5-3.8 1 .1-5.6-2.5-5.8-7.9.4-21.2-7-29.4-3.4zm-145.7 30c1.2 3.7 2-1.2 2.5-1.7 7.7-6.6 19.1-17.3 19.1-27.5-7.6 9.3-13.2 20.6-21.6 29.2zm68.3-.9c1.6-5.6 9-9.9 9.2-14.1-9.4 6.2-13.7 17.3-23.3 23.3 3.9-7.7 9.5-13.8 15.8-19.1-2.5-.6-2.4 1.4-5 .8.1 2.1-1.3 2.6-1.7 4.2-14.8 9.9-27.2 26.6-39.1 36.6 8.8-3.7 17-12.4 24.1-15-6.6 5.9-13.8 11.1-20 17.5 8.3-2 14.3-10.7 21.6-10.8-1.1.6-4.3 3.4-1.7 5 4.9-2.3 9.9-9 14.1-9.2-6.3 7.5-18.5 13.7-24.1 19.1 10.7-1 16.4-11.4 25-11.7-6.5 6-15 9.9-21.6 15.8 7.8-.8 10.1-7.1 18.3-7.5-2.3 4.1-8.2 4.6-10.8 8.3 10.5-3.9 19.2-9.6 26.6-16.6.2 2.9 3.1-2.1 3.3.8-7.1 7.3-16.7 12.2-25.8 17.5 7.3-.8 13.1-7.4 20-5.8-2 2.7-10.4 3.4-9.2 6.7C11.8 408 17.5 398 26 397.8c-2.3 4.6-9.3 4.6-10.8 10 9.1-1.2 11.7-13.3 20-12.5-3.5 8.2-14.7 13.1-17.5 19.1 9.8-5.2 15.8-14.2 25-20C39 404.1 27.2 410 23.6 417c3.1-3.1 7.4-4.3 10.8-7.5 5.5-5 8.8-14.4 15.8-15-5 8.5-12.3 14.9-19.1 21.6 11.1-5 16.3-15.9 25-23.3-2.2 10.3-13.7 15.7-16.6 22.5 9.6-5.6 14.5-16 23.3-22.5-3.2 10.4-14 17.7-18.3 24.1 11-7.5 19.8-17.4 25-30.8h3.3c-.9 2.9-2.2 5.5-5 6.7-.4 9.3-8.1 13.5-13.3 20.8 13.7-6.4 21.5-21.7 23.3-37.5-5.7-.4-2.2 8.3-7.5 8.3.8-5.9 2-11.3 5-15-1.1-4.3-1.5-3.4.8-6.7-2.2.2-4.2.3-3.3-2.5-2.5-.2-2.8 3.1-5 .8-.1-3.1 3-3.1 3.3-5.8-4 .2-3.2 5.2-8.3 4.2 1.4-4.2 5-6.1 9.2-7.5-2.1-2-4.9 0-7.5 0 .9-3.2 5.2-3.1 6.7-5.8-5.5-.5-6.9 7.5-11.7 5 1.3-3.4 5.5-3.9 7.5-6.7-5.5.9-8.8 8.4-13.3 7.5 2.2-5.3 12.2-7.2 12.5-11.7-7.8 3-12.5 9.1-20 12.5 4.5-7.4 13-10.9 19.1-16.6-10 3.1-15.4 10.7-23.3 15.8-2 2.5-3.9 9.4-8.3 6.7 5.9-8.3 14.6-13.7 21.6-20.8-11.8 4.3-17.7 14.4-26.6 21.6 3.6-9.7 12.7-14 19.1-20.8-2 0-2.5 1.4-5 .8-2.1 4.8-7.9 6.7-11.7 10.8-2.8 3.1-3.9 10.8-9.2 9.2 4.7-8 11.7-13.9 18.3-20-9.1 2.5-12.9 10.4-19.1 15.8-.8 2.3-2.4 8.1-5.8 5 3.6-6.1 8.2-11.3 13.3-15.8-10.5 3.5-12.7 15.2-21.6 20.1zm-41.6-26.6c-7.7 10.9-15.7 21.5-25 30.8 10.5-6.9 23-20.8 25-30.8zm35.7 14.2c7.9-2.1 15.4-9 16.6-15-5 5.4-11.7 9.2-16.6 15zm-29.9.8c-8 8.4-22.8 10-26.6 22.5 4.4-2.8 8.9-10 13.3-10-3 5.8-9.7 8.1-12.5 14.1 5.5-.1 5.2-5.9 10-6.7-1.1 4.2-4.6 5.9-5.8 10 5.2.2 5.3-7.6 10-6.7-3.3 4.7-4.4 6.5-7.5 10.8 4.7.3 4.5-4.3 9.2-4.2-1.6 2.9-7.6 5.7-6.7 8.3 4-2.4 6.4-6.4 11.7-7.5-1.5 4.1-6.2 4.9-8.3 8.3 7.3-.7 10-6.1 15.8-8.3 5.1-9.4 18-15.3 20.8-24.1-5.8 2-17.1-5.5-26.6-.8 5.1-6.3 16.6-10.5 17.5-18.3-4.5 3.3-8 11.9-14.1 10.8 1.3-2.8 5.2-3.1 5.8-6.7-5.3 2.2-10.2 9.2-15 9.2 2.4-2.6 4.1-5.9 5.8-9.2-5.9 6-13.5 14.8-17.5 20 6.7-5.1 12-11.9 20.7-11.5zm11.6 3.3c7.5-3.6 14.2-8 17.5-15.8-5.4 5.1-14.8 10.7-17.5 15.8zm8.4-.8c6.8-3.2 15.3-9.1 16.6-15-5.8 4.7-12.4 8.7-16.6 15zm-14.2 0c4.7-1.6 11.7-8.3 15.8-13.3-6.3 3.4-12 7.3-15.8 13.3zm137.3-3.3c2.4 1 3.8.4 4.2-2.5H116c.3 1.6-1.1 1.6-1.7 2.5zm-29.1 27.4c-1.4-4.3-3.5-8-2.5-11.7 1.7-6.3 7.5-5 10-9.2-12.1-.7-15.7 14.7-7.5 20.9zm33.3-12.5c-.2 1-3.7 3.5 0 3.3.9-1.6 2.1-2.9 2.5-5-7.5-1.6-18.4-6-25-3.3 8.9.3 16.5 1.9 22.5 5zm-11.6 23.3c-1 .1-2.4-.1-2.5.8 14.7 5.8 32.2 1.7 45.8 5.8 1.3-2.9 5.9-6.9 7.5-6.7-.5 4-3.7 5.2-6.7 6.7 4.9-.7 10.2 2.6 14.1 0-4.5-.9-6.8.8-9.2-1.7 3.8-2.3 4.1-8.1 9.2-9.2-1.2 1.6-3.6 6.3-2.5 7.5 3-2.9 4.4-7.2 8.3-9.2.1 2.1-3.5 4.7-1.7 5.8 5-7.1 11.7-14.5 10.8-25 1.1-2 3.1.9 3.3-.8-1.2-.2-.8-2-.8-3.3-12.1-3.1-20.9 5.4-32.5 6.7-7.6.8-13.9-2.5-22.5-.8-4.8 8.2-7.3 18.7-15 24.1 0-1.1.4-1.8.8-2.5-8.1.4-15.3-4.7-20.8-4.2 4.2 4.8 9.1 3.2 14.4 6zm.8-20.8c-6.1.8-7.8-2.7-14.1-1.7 1.6 3.7 9.4 1.1 10.8 5-4.4.5-8.9-3.3-12.5-.8 8.1 1.6 14.1 5.3 23.3 5.8-.9-2.6-.1-1.9 0-5-1.3-1.2-4.3-.7-5-2.5 1.5-.2 4.2.8 3.3-1.7-5.4-.5-4.5-2.2-10-2.5-.5 3.1 4 1.1 4.2 3.4zm12.5 5c2.2-1.3 6.6-6.8 3.3-8.3.5 4.2-4.5 6.1-3.3 8.3zm49.1 19.2c6.2-5.2 15.4-16.1 14.1-23.3-2.7 9.7-9.2 15.7-14.1 23.3zm-56.6-8.4c-.3-2 1.5-1.8.8-4.2-7.3-3.3-17.8-7.7-26.6-6.7 12.1.2 11.9 12.6 25.8 10.9zm-20.8-2.5c-1.7-.3-8.2-2.7-8.3.8 2.2-1.9 7.1.6 8.3-.8zm-14.2 7.5c-1.2.5-2 1.3-2.5 2.5-.3 5.8-6.6 12.8-10.8 17.5-2.5.5-2.7 4.3-.8.8 5.2-1.4 7.1-6.3 11.7-8.3 2.1-4.3 3.2-9.6 6.7-12.5.4 11.5-10.3 19.1-17.5 25.8 8-1.8 10-9.4 16.6-12.5-2.4 6-7.1 9.5-10.8 14.1 9-3.5 14-15.4 20-19.1-1.3 5.4-4.1 9.2-6.7 13.3 5.8-3.6 9.9-9 10.8-17.5-5.3 1.5-2-.3-5-1.7-1.9 3.1-1.8 8.2-6.7 8.3 1.2-4.1 3-7.6 4.2-11.7-2.6-1.1-2.8-.2-5.8 0v-5.8c-4.5.6-2.4 2.7-3.4 6.8zM-30.5 402c4.8-3.5 12.6-4 15.8-9.2-5.6-.9-13.7 6.6-15.8 9.2zm114.9 16.6c6-3.2 19.5-18.4 11.7-22.5-1.7 9.3-7.8 18.4-11.7 22.5zm8.3-1.6c6.2-2.7 11.5-10.7 10-18.3-2.2 7.2-6.2 12.6-10 18.3zm-203.9-8.4c-3.9-.6-5.4-7.8-9.2-5.8 3.3 1.2 5.1 8.3 9.2 5.8zm211.4 8.4c2.7-4.8 7.4-7.6 7.5-15-4.5 2.5-6.4 11.9-7.5 15zm25-1.7c2.6.4 8.3-7.8 7.5-10.8-2.7 3.5-4.9 7.4-7.5 10.8zm-20.8.8c4.2-.2 2.9-6 6.7-6.7.9 5.3-2.3 6.6-3.3 10 6.7-.6 5.8-9.8 1.7-13.3-1.1 4.1-3.7 6.5-5.1 10zm14.1-3.3c.7-2.1 5.3-4.6 3.3-6.7-.1 2.8-4.8 5.1-3.3 6.7zm4.2-.8c4 .4 3.2-4 5-5.8-2.1 1.5-4.7 2.5-5 5.8zm6.6 4.1c3.5-1.9 10.9-6.4 7.5-10-2.1 3.8-5.6 6.1-7.5 10zm9.2-1.6c1.4-2.8 7.3-5.4 5.8-8.3-1 3.1-6.7 6-5.8 8.3zM7 412.8c3.1-1 6.4-1.9 7.5-5-2.9 1.3-5.5 2.9-7.5 5zm-69.1 12.5c4.9-.1 8.2-1.8 10-5-3.8 1.2-7 3-10 5z" />
                                <path d="M69.4 194.7c0 3.6-4.9 2.3-5.8 5-2-2.3 4-3.7 5.8-5zm-10 79.9c-1.6 8.1-4.1 22.5-11.7 27.5 3.9-9.2 7.6-18.5 11.7-27.5zM4.5 298.8c-8.2 14.7-13 27.9-26.6 37.5-.8-2.9 3.3-5.3 5.8-8.3 2.9-3.4 6.6-7.8 9.2-11.7 4-6.1 5.3-14.5 11.6-17.5zm-144 39.9c-5.9-8.8-5-21.2-5.8-30.8 2.7 11.9 5.9 19.8 7.5 33.3h-2.5c3 .8 3 4.8 3.3 8.3-3.9-.2-5.1-9.3-2.5-10.8zm267.2 9.2c-2.1 3.2-3.6 6.9-6.7 9.2 1.3-4 3-7.6 6.7-9.2zm-99.9 30.8c.1 2.9-3.7 9-9.2 9.2 2.3-3.9 6.3-6 9.2-9.2zm-6.7 16.6c-4.2-2.6 3.8-8.6 6.7-6.7-4 6-8.7 11.2-17.5 12.5 2.9-2.6 5.3-5.8 10.8-5.8z" />
                            </svg>
                            <span>Register Me</span>
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-152 174.4 494.4 252.6">
                                <title>Layer 1</title>
                                <path d="M-11.4 184.8c.1 1.8-1 6.7-2.5 4.2 2.6-.3-2.2-4.2.8-5 .2.6.8.9 1.7.8zm-81.5 5.8h-5c.5-3.2 4.6-2.7 5 0zm223 3.3c-4.3-.4-9-.4-10-4.2 4.5.3 8 1.6 10 4.2zm52.5 54.1c-12.4-15.9-23.7-32.9-40-44.9-1.8-1.8-6.2-1-5.8-5 22.2 9.6 34.9 28.9 45.8 49.9zm-62.5-17.5c-7.6-5.4-13.6-12.5-20-19.1 9.5 3.6 15.3 10.8 20 19.1zM334 213.9v3.3c-8.1-.9-22.6 2.4-29.1 10-1-.4-1.9-.9-2.5-1.7 5.5-8.4 19-13.2 31.6-11.6zm-237.2 10c-2.3-.8-3.5-2.6-4.2-5 1.4 0 2.1-.6 2.5-1.7-5.1.5-.5-5.9 3.3-4.2 1.8 5.7 11.1 8.3 10 14.1-3.5-2.9-8.4-12.3-12.5-9.2 3.3 1.7 7.6 6.8 6.7 10-.6-1.8-7.6-7.4-5.8-4zm243.1-.9c.5 10.1 4.8 26.9-.8 34.1.8-10.4-2.8-27.2.8-36.6 1.2 1-.1 2 0 2.5zm-16.7 5.9c-9.3 2.4-19 8.7-20.8 15.8 4.9-2 11-2.8 15.8-5 .1 6.9-12.2 8.3-15 15 4.5.2 5.2.4 8.3.8.1 6.5-5.1 7.6-9.2 10 .5-4.5 6-4 5.8-9.2-2.7 1.1-8.6 1.8-9.2-1.7 2.3-4.7 11.1-7.2 11.7-10.8-3.6 1.3-9.8 3.8-13.3 2.5 1.5-7.7 9.9-8.4 12.5-15-2 .3-3.1 1.3-5.8.8 3.7-4.6 12.8-3.8 18.3-6.7-.1 1.7 1 2 .9 3.5zM13.6 244.7c-.6-2-7.3-5.5-2.5-7.5-.2 3.1 5.1 4.8 2.5 7.5zm84.1.8c-2.8 3.1-8.1 8-7.5 11.7 5.6-.3 11.5-4.2 17.5-8.3-5.6 8.6-13.9 14.4-27.5 15-3.1-3.6-4.8-8.6-6.7-13.3 5.3.7 4.2 11.7 9.2 10-1.1-3.4-3.9-4.9-4.2-9.2 4.4.4 4 9.8 9.2 6.7-1.1-2.2-4.4-2.3-4.2-5.8 2-2.8 3.7 4 5 1.7-2.3-4.4 6.3-12.1 9.2-8.5zm227.2 15c1.5.2 4.2-.8 3.3 1.7-1.2 2.3-2.5 1.4-4.2 0-2.6 3.2-2.5 9.2-6.7 10.8 2.8-7.2 6-21.2 11.7-28.3 1.2 7.8-2.2 11.1-4.1 15.8z" />
                                <path d="M314.9 289.6c7.5-13.6 14.2-28 20-43.3-1.3 19.9-11.7 30.5-20 43.3zM91 266.3c3.9-1.3 6.3-4.2 10.8-5 3-4.4 8.7-14.2 13.3-11.7-3 3.4-9 8.2-10 10.8 3.5-1 6.6-6.7 10-5-4.5 6.2-16 13.9-24.1 10.9zm-44.1-5.8c-4.7-2.3-6.7-7.2-8.3-12.5 4.9 2.1 6.1 7.7 8.3 12.5zm31.6 4.2c-5.3.8-11.8-12.3-10-15.8 3.1 5.4 6.3 10.9 10 15.8z" />
                                <path d="M73.5 265.5c-8.3-1.4-9.7-9.7-12.5-16.6h3.3c1 7.6 6.5 10.8 9.2 16.6zm-20-4.2c-6.1-.7-9-8.5-9.2-12.5 4.1 3.2 7 7.5 9.2 12.5zm-95.7-9.1c.1-2.7 3.8-1.8 5.8-2.5 0 2.7-3.7 1.8-5.8 2.5zm101.6 10c-5.6-1.1-6.5-6.9-8.3-11.7 4.5 2.1 6.8 6.4 8.3 11.7zm5 0c-5.2-.6-4.5-7.2-7.5-10 1.1 0 1.2-1.1 2.5-.8 1.2 4 4.6 5.9 5 10.8zm-52.5 0c-5.3-1.6-9.4-4.4-12.5-8.3 5.1 1.8 9.7 4.1 12.5 8.3zm-5 .8c-4.9 1.6-9.1-4.1-12.5-6.7 4.3-1 9.2 4.1 12.5 6.7zm-14.1-4.2c3.8 1.5 7.9 2.6 9.2 6.7-5.1-.3-7.1-3.5-9.2-6.7zm349.6 15c-2.4-3-3.9.6-5.8-.8 5-.8.1-11.5 5-12.5 1 2.6.7 7.7.8 13.3zm-362.9-10c2.6 1 4.1 3.1 7.5 3.3-.6-3-4.8-2.4-5-5.8 5 1.1 7.3 4.9 12.5 5.8-1.3-3.1-5.7-3.2-7.5-5.8 5.2.6 9 2.7 11.7 5.8-8.5 1.3-15.2 4.3-24.1 1.7 2.8-.3-3.2-2.6 0-3.3 1.9 1.2 7.1 3.4 8.3 2.5-.9-1.2-5.7-2.7-3.4-4.2zm-5.9 5.9c-3-.1-6.7.6-6.7-2.5 1.9 1.2 6.3-.1 6.7 2.5zm56.6 4.1c-7.3-1.2-11 5.6-16.6 3.3 3.8-2.3 13.6-8.2 16.6-3.3zm106.6 27.5c-6.8-6.5-9.3-17.3-11.7-28.3h2.5c.6 11.9 6.5 18.5 9.2 28.3zm-5-1.7c-7-4.1-11-20.9-10-25.8 3.7 8.2 6.1 17.7 10 25.8zm-98.2-22.5c-6.4-1.5-11.1 6-15 1.7 5 .4 10.9-6.6 15-1.7zm302.1 26.7c.4-7.2 1.5-17.7 2.5-25 2.4.7 2.2-1.2 4.2-.8-3.1 8.9-1.4 21-6.7 25.8zm-24.1-20c-.2-2.7 1-4 3.3-4.2-.5 2.1-.7 4.3-3.3 4.2zm-163.2 19.1c-6.8-2.7-10.3-17.4-9.2-22.5 2.1 8.6 6.6 14.6 9.2 22.5zm5-15.8c6.4 15.8 14.8 29.5 23.3 43.3-13.8-9.3-17.6-28.4-25.8-43.3h2.5zm64.9 17.5c-2.3-4.9-9.5-9.3-8.3-15 2.2 5.1 8.9 10 8.3 15zm-46.6 10.8c3.1 4.7 11.7 14.1 10.8 16.6-12.7-8.9-16.4-26.9-24.1-40.8h2.5c2.7 9.1 8 15.5 10.8 24.2zm20 18.3c-7.6-1.7-12.3-9.6-15.8-15.8-4.2-7.5-6.8-17.3-10.8-25 .2-.6.7-1 1.7-.8 8 15.9 14.6 29 24.9 41.6zM43.6 299.6c-3-.9-4.9-2.9-5.8-5.8 3.5.3 5.5 2.2 5.8 5.8zm142.3 19.2c-5.1-5.5-12.6-17.2-11.7-23.3 3.8 7.8 8.5 14.8 11.7 23.3zm5.8-.9c-6.2-4.3-11.4-14.1-12.5-20.8 4.5 6.6 8.9 13.4 12.5 20.8zm16.7-10.8c-.3-1.4-3.9-3.7-1.7-5 .3 1.4 3.9 3.8 1.7 5zm-.9 29.1c-8.5-2.1-13.7-11.8-15.8-17.5 7 4.2 10.5 11.7 15.8 17.5zm-6.6-.8c-9-1.3-12.3-8.2-15-15.8h2.5c2.5 6.9 8.7 10.1 12.5 15.8zm61.6 35c6.5-12.7 9.2-29.1 12.5-44.9 1.1 0 1.2-1.1 2.5-.8-4.8 12.4-4.9 38.8-15 45.7zm-199.8-25c-5.3-3-4.9-11.8-8.3-16.6 5.7 2.1 7.7 12.1 8.3 16.6zm20-3.3c-1 .7-2.2 1.1-4.2.8 0-2.9 3.4-2.7 4.2-.8zm-39.1 10c-3.6.8-2.8-5.3-1.7-6.7 2 .5 4.3.7 4.2 3.3-2.2-.2-2.8 1.2-2.5 3.4zm21.6 4.9c-4.3-1-5.6-4.9-7.5-8.3 1.1 0 1.2-1.1 2.5-.8 1.2 3.5 4 5.4 5 9.1zm79.1 10c-7.1-2.4-9.4-9.5-14.1-14.1 1.1 0 1.8-.4 2.5-.8 3.5 5.2 9.5 8.1 11.6 14.9zm-10-5.8c-3.8-.6-9.7-6.2-6.7-8.3 2 3 5.5 4.5 6.7 8.3zm14.1 0c-1.8-2.1-4.2-3.6-4.2-7.5 4.1-.5 7.5 6 4.2 7.5zm-3.3 1.7c-3.9-1.9-5.7-6-8.3-9.2 5.5.3 7.4 4.2 8.3 9.2zm180.6 6.6c-.6-5.9 1.3-9.3.8-15h2.5c-.9 5.2-1 11.2-3.3 15zm-168.1 15.8c-8.1-.5-10.4-11.2-10.8-16.6 3.6 5.5 6.8 11.4 10.8 16.6zm-94.9-9.1c-.9-2.7-2.4-4.8-3.3-7.5 3 .3 6.5 6.3 3.3 7.5zm159.8 5.8c-3.7-3.6-10.8-8.1-10.8-12.5 3.7 3.6 10.1 8.8 10.8 12.5z" />
                                <path d="M65.2 377c2.6 4.6 5.3 9.2 7.5 14.2-5.6-1.1-4.6-8.8-9.2-10.8.5 4.8 3.5 7 5 10.8-4.2 0-3-5.3-6.7-5.8-.4 3.2 2.1 3.4 2.5 5.8-6.7-5-9.2-14.1-13.3-21.6 6.7 1.9 5.5 11.7 11.7 14.1-1.9-4.2-6.9-9.8-5.8-14.1 3 2.4 3.9 11.5 8.3 7.4zm97.4 8.3c-6.2.8-8.6-9.1-11.7-14.2h3.3c.1 7.5 6.6 8.5 8.4 14.2zm-36.6 0c-5.2-1.2-5.3-7.4-5.8-13.3 4.4 1.9 4.2 8.5 5.8 13.3zm-73.3-4.1c-1.6-3.1-3.6-5.8-5-9.2 3.5.7 7.9 7.4 5 9.2zM245 402.8c.2-4.9 5.4-8.5 8.3-13.3 3-5 5.5-10.9 8.3-15.8.2-.6.7-1 1.7-.8-4.9 11.2-9.2 22.9-18.3 29.9zm17.5-20.8c1.7-2.2 3.4-8.7 5.8-7.5-2.8 1.6-.9 8.1-5.8 7.5zm-33.3 0c-4.7-.9-6.9-4.2-8.3-8.3 3.7 1.8 6.4 4.6 8.3 8.3zm-98.2.8c-3.9 1.3-6.6-6.9-3.3-9.2 1.2 3 1.9 6.5 3.3 9.2zm17.4 1.7c-3.8-.4-6.4-6.3-5-9.2 1.7 3.1 4.3 5.2 5 9.2zm-11.6-.8c-4.2.6-5.3-6.4-4.2-8.3 1.2 3 3.7 4.6 4.2 8.3zm52.4 5.8c-6.3-1.5-14.8-9.5-13.3-13.3 3.5 5.3 10 7.8 13.3 13.3zm77.4-7.5c.5-2 .7-4.3 3.3-4.2-.4 2.1-.7 4.3-3.3 4.2zm-64.9 4.2c-4.7.3-8.6-4.7-7.5-7.5 2.3 2.7 5.7 4.3 7.5 7.5zM80.2 382c3.1-.8 5.4-2.4 9.2-2.5-.5 3-6.8 4.4-9.2 2.5zm-30-1.7c0 1.1.4 1.8.8 2.5-1.4 1.2-4.6-.9-5.8-2.5 1.8-1.8 2.4.5 5 0zm262.2 8.4c.3-3.3 2.7-4.5 4.2-6.7.1 3.8-1.8 5.5-4.2 6.7zm-262.2-4.2c5.9 1.3 2.2-.7 5-2.5 1.2 3.3 3.7 5.2 4.2 9.2-4.9 2.6-8.5-2.4-9.2-6.7zm141.5 13.3c-7.7-1.2-13-9-19.1-14.1 7.3.5 13.1 9.5 19.1 14.1zm-54.1-9.1c-.3-1.6-3.2-.7-2.5-3.3 1.1 0 1.2-1.1 2.5-.8.5.4 2 3.9 0 4.1zm16.7 3.3c-5.7 1.3-4.3-4.6-6.7-6.7 3.3 1.2 5.7 3.2 6.7 6.7zm-8.4-2.5c-2.2-.3-2.8-2.2-3.3-4.2 2.2.3 2.8 2.2 3.3 4.2zM52.7 392h-2.5c-1.8-1.3-2.4-3.7-3.3-5.8 3.5.4 4.6 3.1 5.8 5.8zm113.2 3.3c-6.3.2-8.1-4.1-10-8.3 6.3-.1 6.4 5.8 10 8.3zm-36.6-3.3c-3.3 1.3-6.3-4.6-2.5-5 .3 2.2 2.5 2.5 2.5 5zm169.8 11.7c2.3-6.5 8.1-9.7 12.5-14.1-3.4 5.5-6.7 11-12.5 14.1zm-249.7-10c-1.6-.2-1.8.9-3.3.8-.7-1.8-1.3-3.7-2.5-5 2.9.4 4.5 2.1 5.8 4.2zm-5-.9c.8 2.5-1.9 1.5-3.3 1.7V392c1.5-.2 1.8.9 3.3.8zm235.5 31.7c5.1-4.8 10.6-9.4 16.6-13.3-3.2 6.1-9.9 13.2-16.6 13.3zm16.7 1.6c-3.6.4-2.7-2.6-3.3-3.3-1.3 1.8-3.1 3-6.7 2.5 2.5-3.4 7.3-8.8 10.8-8.3-3.5 3.2 1.8 5.5-.8 9.1z" />
                                <path d="M190 250.5c-7.6-9.9-15.1-19.8-21.6-30.8-21.8-26.8-58.9-44.2-102.4-40.8-11.1.9-22.2 2.9-33.3-1.7-15.6 5.4-37.1-1.3-48.3 4.2-1.6.2-1.6-1.1-2.5-1.7-19.8 2.6-43.1 1.2-60.8 9.2-21.1-3.2-45.6 0-65.8 7.5 3.7-7.7 15.1-4 20-9.2 32.7-2.6 64.8-11.8 99.9-10 2.3-.5 3.5-2.1 5.8-2.5 15.4-.9 30.4.5 44.9.8 24 .5 48.4-1 72.4 1.7 17.9 2 32 10.1 47.4 15 41.3 13.2 80.5 26.4 110.7 49.1 2.8-23.3 27.8-19.8 45.8-20.8v4.2c-2.4-.7-2.2 1.2-4.2.8-.1-1 .1-2.4-.8-2.5-2.6 7-10.7-.2-15 3.3 1.8 4.9 12.4.9 14.1 5.8-2.8.4-9.4 0-7.5 2.5-2.8.8-3.6-4.6-5-2.5 7.6 17.7 13.5 40.8 6.7 59.1 1.7 2.3.9 7.7-2.5 7.5-.3-8.8 3.7-23.7 0-30-.5 14.2-3.3 25-5.8 35.8-1.4 6-2.6 12.3-5.8 15.8 2.3-22.8 15.8-46.9 8.3-69.1-4.8-2.2-8.7-.2-13.3-2.5 3.6 41.5-.7 97.3-21.6 133.2-3.3 1-6.1-2.2-6.7.8 1.8 1.9 2.8-.7 5.8 0-3 6.4-9.3 14-10.8 19.1 15.2-18.4 24.6-42.3 31.6-66.6.5-1.9-1.3-5.2 2.5-5.8-6.4 29.7-16.1 56-32.5 75.7 5.3.1 7.7-.5 10.8-.8-2.4-2.6 2.2-6.8 5-11.7 2.3-4 3.8-8.2 6.7-10 .9 3.6-4.4 7.7-5 12.5 4.6-2.3 5-8.8 9.2-11.7.2 8-8.4 14.3-14.1 19.1 1.3 2.8 3 2.8 5 3.3v-3.3c4.2-.1.2 2.8 3.3 3.3.9-2.4 6.1-.6 6.7-3.3.7-3.8-.9-2.4-1.7-3.3 4.2-4.6 1.4-11 5.8-15 2.1-23.9 9.5-49.6 14.1-70.7.9-4.2-.3-11 4.2-13.3-6 35.6-18.2 75.3-19.1 114.9 12.9-34.3 19.3-76.2 27.5-114.9.3-1.2 1.6-2 1.7-3.3.5-5.1-2.5-15 1.7-18.3 2.8.9-.3 3.2 1.7 4.2 6.7-3.8 7.7-17.9 14.1-19.1-3.5 12.3-16.1 20-16.6 32.5 4.7-2.8 5.9-9 10.8-11.7-2.2 9.4-9.5 13.3-10 20.8 3.9-2.7 5.4-7.9 10-10-3 9-11.6 12.2-12.5 23.3 15.3-13.8 22.6-35.7 35-52.4-3 11.3-11.2 21.3-15.8 33.3-7.5 9.5-19.3 19-20.8 31.6 12.5-13.3 21.9-27.3 29.1-44.1 2.6.9 2.2.4 4.2-.8-5.2 18.3-4.6 40-5.8 60.8-.5 9.1-3.4 18.4.8 25 3.6 17.6-15 32.3-18.3 44.9-3 1.3-2.2 1.5-5 3.3-3-2.9 2.4-6 0-8.3-1.7.2-1.6 2.3-3.3 2.5 1.1-2.4-.5-4.3 0-8.3 11.1-8.1 21.1-17.5 20-34.1-4.6 10.1-12.1 17.3-20 24.1-1.3-2.9 1.9-4.9 5-5.8 4.5-8.2 14.6-15.3 15.8-24.1-8.3 6.2-13.7 19.6-22.5 22.5 7-10.3 19.6-19.2 23.3-30-9.7 5.5-14.1 16.4-23.3 22.5 3.3-7 12.8-15.2 18.3-22.5 2.4-3.2 8.2-9 3.3-11.7-10.5 15-22.6 28.4-36.6 40-.6 1.9-3 6.3-1.7 7.5 6.2-3.2 8.2-10.6 15-13.3-3.3 9.4-17.1 16.3-16.6 24.1 5.9-3.3 10.8-16.3 17.5-13.3-5.3 3.9-16.5 12.7-17.5 20.8 6.1-5.5 11.1-12.3 17.5-17.5-4.4 13.7-18.7 17.4-25.8 28.3 10.2-4.5 16-13.4 25.8-18.3-5.8 7.8-14.7 12.5-20.8 20-4.1-.4-10.6-1.2-13.3-2.5 2-2.5 4.8-4.1 5.8-7.5-2.9 3.6-10.7 5.5-8.3 11.7-2.9.5-2-2.9-5-2.5-13.8 9.1-19.3-10.3-25-17.5-9.4 3.9-19.2 7.5-31.6 8.3-29.6 10.6-60.7 7.2-95.7 8.3-30.8 1-61.9-6.2-83.2-18.3-6.2-11.3-16.2-18.8-18.4-34.1 1.9-4.2 3.8-8.4 5-13.3-9.9-2.2-6-13.1-11.7-19.1-5.2.2-5.7-4.3-9.2-5.8.4-10.6-3.1-22.9 4.2-29.1-8.2-7.6-17.7-14-19.1-28.3-9.1-2-17.5-4.6-25.8-7.5-8-8.1-11.3-25-1.7-32.5 7.3-5.7 16.2-3.6 24.1-8.3-7.1-2.3-11.2.8-15-3.3 8.1.5 20.8 2.4 27.5-.8-5.1 1.7-5.1-1.4-8.3-3.3-.5 2.7 1.6 2.6 0 4.2-4-.4-3.9-4.9-7.5-5.8.9 1.9 2.9 2.7 2.5 5.8-4.9 1-4.8-3-7.5-4.2-.4 2.6 2.9 2.2.8 4.2-3.8-.3-5.3-3-7.5-5-.3 1.7 4.6 4.2.8 4.2-3.3 1.5-4.3-3.3-5.8-.8-.2 2.2 3.2.7 2.5 3.3-31.5-5.1-55.9 3.1-84.9.8-9.7-5.1-25.1-17.3-10.8-27.5-.4 4.1-3.4 5.4-2.5 10.8 5.8 0 5.3 6.3 9.2 8.3 0-1.1-.4-1.8-.8-2.5 2.5-2 3.4 2.8 5.8 3.3.1-1.3-.1-2.9 1.7-2.5 5.4 3.3 11.2-.9 15.8 3.3-.1-2.1-1.6-2.6 0-4.2 3.1-.9 2.4 2.1 5 1.7.2-1.5-.8-4.2 1.7-3.3 1.9 1.5 2.8 3.8 5.8 4.2-1-1.3-2.7-3.2-.8-5 3.6-.3 2.1 4.6 5.8 4.2 0-2.7-2.4-3.2-1.7-6.7 4.2 0 4.3 4 7.5 5-.6-2-4-5.9-.8-6.7 1.7.9 3.5 5.8 5 4.2-2-5.6 5.7-9.3.8-15.8 4.4.3-.4-3.4 4.2-2.5 3.3 6.6-.3 13.7 5.8 19.1.7-3.4-1.3-4.3-.8-7.5 4.9-.4 4.2 9.1 7.5 7.5-.1-3.1-3.2-6.2-1.7-8.3 3.3 1.1 2.7 6.1 5.8 7.5.2-3-1.5-4-1.7-6.7 4.6.4 3.6 6.4 7.5 7.5-.6-2-2.4-5-.8-6.7 4.9-1.3 3.5 8.1 6.7 5.8.4-2.6-1.5-2.9-.8-5.8 4.8-.9 4.6 7.6 7.5 5.8-.1-2.4-1.5-3.5-1.7-5.8 6.8-1.5 3.4 7.1 9.2 6.7-.7-2-1.9-3.6-1.7-6.7 3.8 0 2.5 5.3 5.8 5.8.6-2.5-1.4-2.4-.8-5 3.6-1.3 4.2 4.6 5.8 2.5-1-1-.9-2.9-.8-5 2.8-.8 3.6 4.6 5 2.5-1-.1-.9-1.3-.8-2.5 3.1-1.1 4.8 3.5 5.8 1.7-.7-.6-2.2-5.8.8-5 3.1.8.4 7.4 5 6.7-1.2-4.8-3.1-15.2 1.7-18.3 5.2 8.1 7.4 19.2 16.6 23.3-5.9-5.5-9-13.7-12.5-21.6 8.7 1.8 6.5 14.6 14.1 17.5-1-3.9-5.1-8.5-4.2-11.7 5.6 2.2 5.5 10 10 13.3-.4-4.9-3.7-6.9-4.2-11.7 4-.1 2.9 4.8 5.8 5.8 1.3-1.2.2-4.7 3.3-4.2 2.4 2.6 3.1 6.9 6.7 8.3-.8-2.5-2.9-3.7-2.5-7.5 4.4 0 5 3.9 7.5 5.8 0-2.7-2.4-3.2-1.7-6.7 5.6-.3 4.9 10 9.2 8.3-1.1-1.6-4.4-6.3-.8-7.5 2.8 1.7 3 5.9 6.7 6.7-1.2-2.4-3.1-4.1-3.3-7.5 4.3.3 2.6-.9 8.3-1.7-.9-2.4-.5-3.1 1.7-4.2 4.2 1.9 2.9 6.8 5 9.2 1.5-1.8 1.4-5.3 5-5 1.5 3.1-2.6 5.6 1.7 7.5 6-3.2 7.5-10.8 14.1-13.3-.1 4.7-4.1 5.3-4.2 10 3.7-1 5.6-3.9 9.2-5 .3 3.6-2 4.6-2.5 7.5 2.5.6 2.4-1.4 5-.8.8 1.6-2.5 2.8 0 3.3 13.5-5.2 37.2-4.9 49.9-1.7-1.8-3.4-6.9-3.7-8.3-7.5 10.3 2.7 19.6 10.9 27.5 13.3-3.5-5.1-10.5-6.7-14.1-11.7 17.5 7.2 32.7 23.9 41.6 42.4-8.5-8.7-14.3-20.1-25.8-25.8 5.4 7.6 15.8 14.7 17.5 23.3-7.9-9.3-15.1-19.3-25.8-25.8 7.4 7.8 15.2 15.3 20.8 25-10.8-7-15.1-20.4-27.5-25.8 7.1 7.4 14.6 14.2 19.1 24.1-9.9-8.1-17.9-22.6-29.1-26.6 7.9 7 15.2 14.7 20.8 24.1-9.1-7-13.8-18.4-25-23.3-1-2.6 1.3-2 1.7-3.3-2.4-.2-4.2.2-5 1.7 7.4 4.8 13.6 10.8 17.5 19.1-7.7-5.4-12.4-13.7-20.8-18.3.9-2.3-3.2-5.3-5-1.7 8.9 4.4 14.1 12.5 19.1 20.8-7.6-5.8-11.7-14.9-20.8-19.1-1.1-2.1 3.3-2.6.8-3.3-34.2 7.6-82 6.7-114.9 14.1-5.4 2.9-11.7 5-15.8 9.2-7.5.1-20.1.5-25 5.8 8.1-2.1 19.1-1.5 26.6-4.2 6.4 3.3 10.9 9.2 10 17.5-4.5.5-4.6 5.4-7.5 7.5-15.4 8.8-38.5 8.7-38.3-11.7-3.5 13.1 8.3 21.9 20 20 14.1 9.9 36.6.5 50.8-2.5-.9-3.1 1.8-2.7 2.5-4.2-4.7-1.7-8.5-4.3-10.8-8.3 4.4-1.7 9.4 7.1 13.3 5.8-2.6-3.5-7.6-4.6-9.2-9.2 2.6-1.9 6.4 5.5 9.2 3.3-1.7-3.3-7.3-2.7-8.3-6.7 2.3-2.3 6.6 5 8.3 3.3.8-3.4.9-4.3-1.7-5.8 3.3 1.3.3-3.6 2.5-3.3 2.9 2.1 1.4 8.6 4.2 10.8.7-5.1.7-11 1.7-15.8 4.2 1.3 2 9.1 3.3 13.3 1.1-.3 1.6-1.2 3.3-.8-.9 5.9 4.8 9.6 6.7 10-1.2-3.8-3.9-6.1-5-10 4.4 2.7 9.2 8.9 9.2 13.3-3-2.2-4.7-2.4-6.7-.8 9 6.3 20.4 3.8 30 5 5.7.7 10.6 4.1 15.8 4.2 3.2 0 6.5-3.2 9.2 0-2 1.5-5.6-.5-7.5 2.5 10-.6 28-6 38.3-8.3-2.5 5.6-11.7 4.4-15.8 8.3 3.9-.3 11.3-1.9 15.8-4.2-1.6 7.5-14 4.3-20 7.5.9 3.3 4.7 8.1 6.7 7.5-2-1.9-4-3.8-4.2-7.5 4.4-.3 2.2 3.6 6.7 1.7.2 2.1 1.9 6.9 3.3 5 .1-2.7-3.7-3.7-1.7-5.8 4.7.6 2.8 7.8 6.7 9.2.1-3.7-2.3-4.9-1.7-9.2 4.2 7.2 6.7 16 10 24.1-5.7-.9-4.1-9.2-8.3-11.7 1.4 8.3 4.8 14.7 10 19.1.4-3.1-4-4-1.7-6.7 3.6.6 2.4 5.9 5.8 6.7-.3-2-4-4.8-1.7-6.7 8.1 6.9 10.5 23.9 20 26.6-4.8-7-14.6-17.5-13.3-25 5.8 9.2 11.3 23.1 20 26.6-4.6-2.2-6.1-8.3-9.2-13.3-2.3-3.7-8.3-8.3-5.8-11.7 6.1 8.6 12.6 21.2 20 25.8-5.3-8.2-12.8-14.4-15.8-25 6.8 5.1 8.9 15 16.6 19.1-8.7-11.2-13.6-26.3-20-40 7.7 3.7 7.5 13.8 10.8 20.8 4 8.4 10.5 15.4 15.8 21.6.2 1.6-1.1 1.6-1.7 2.5 14.6 7.3 35.2 12.6 54.1 7.5h-6.7c-9.2-11.9-16.6-22.7-24.1-37.5 7.8 4.4 8.7 15.7 15.8 20.8-.2 2.4 1.9 2.6 1.7 5 5 2.3 5.4 9 11.7 10-5.5-8.6-13.4-14.9-16.6-25.8-2.8-1.9-7.9-8.6-5-10.8 7.8 12.7 16.4 29.1 27.5 35.8-5.2-6.5-13.3-14.4-14.1-22.5 5.1 7.7 10.8 14.7 17.5 20.8-8.3-13-18.8-23.9-25-39.1 10.6 10.2 15.7 25.9 27.5 35-3.1-6-9.5-13.3-10-19.1 3.6 3.4 6.5 11.8 10.8 11.7-9.2-10.5-15-24.4-22.5-36.6 11.2 8.5 14.7 29.2 25.8 35-24-35.1-31.6-91-64.9-114 17.7 18.1 30.2 41.4 39.1 68.3-16.4-25-23.8-58.8-51.6-72.4 18.8 14.3 31.1 34.9 40.8 58.3-5-.6-4.3-5.8-5.8-9.2-8.7-19-29.1-45.1-47.4-53.3 18.1 14.7 34.2 31.1 43.1 54.6zm-205.5-53.3c-2 7.6 4.5 13.8 9.2 17.5-3.7-5.3-7-10.9-9.2-17.5zm27.4 8.4h-1.7c.1.7-.3.8-.8.8 1.5 1.2 1.2 4.3 4.2 4.2-.5-1.8-2.1-2.4-1.7-5zm169 .8c-1 .1-.7 1.5-.8 2.5 22.1 19.2 31.3 51.4 42.4 81.6 7.7 9.2 10.4 23.5 20.8 30-15.4-20.3-21.2-50.4-35.8-71.6-4.5-18.6-14.5-31.6-26.6-42.5zm-51.6 1.7c.8 2 3.3 2.2 5.8 2.5-1.4-1.4-3-2.6-5.8-2.5zm-144 .8c-1.5 1 2.1 3.4 0 5.8 2.1-.1 2.6 1.3 3.3 2.5 1.7-2.2-2.5-5.8-3.3-8.3zm205.6.8c-.9 3.9 4.9 9.1 8.3 14.1 3.3 4.8 5.7 12.2 10.8 14.2-2.9-12.9-12.4-19.2-19.1-28.3zm58.2 111.6c-.1 7.9 8.5 14.2 11.7 21.6 2.3.6-.6-3.9 1.7-3.3-5.7-5-8.9-12.2-13.4-18.3zm-14.9-30.8c-10.4-14.6-13.3-36.6-24.1-50.8-1 1.8 1.4 9.1 2.5 11.7 10.1 22.9 19.8 52.1 35.8 69.1-4.5-9.6-11.2-17.1-14.1-28.3 10 13 15.3 30.8 26.6 42.4-25.3-35.5-32.7-88.9-61.6-120.7 14.8 21.8 26.1 51.4 34.9 76.6zm-26.7-74.1c3.5 9.3 11.1 21.7 16.6 31.6 11.4 29.4 22.2 63.8 40 84.1 2.3-5-1.3-2.7-3.3-5.8-20.6-33.8-28.5-80.3-53.3-109.9zm8.4 4.1c-.7 4.5 10.2 16.7 10.8 23.3 14.6 24.2 18.5 59.2 37.5 79.1-18.3-29-25.4-75.4-48.3-102.4zm9.1 5c-1.4-.3-1.6.7-1.7 1.7 14.4 23.1 24.5 50.4 33.3 79.1 3.9 1.6 2.5 8.6 7.5 9.2-9.8-27.7-19.1-52.3-30.8-79.1 5.4 4.2 7.8 12.6 10.8 20 2.9 7.3 7.6 14.4 10 21.6 1 3.1.5 7.4 1.7 10.8 2.3 7 4.6 13.3 9.2 18.3-6.7-27.2-17.6-57.3-33.3-78.2-2.1 1.3 1.5 4 1.7 5.8-4.5-1.4-5.1-6.6-8.4-9.2zm46.6 2.5c-1.5 1.9-4.7 1.9-5 5 2.2-1.2 4.7-1.9 5-5zm-33.3 5.9c11.9 23.2 24.6 47.7 29.1 75.7 3.4-2-.4-7 1.7-9.2-.1-1.6-2.8-.6-2.5-2.5-5.7-25.1-14.6-46.9-28.3-64zm28.3.8c-3 1.5-4.5 4.4-4.2 9.2 3.9-.6 1.8-7.1 4.2-9.2zm-297.1 2.5c-1.9-1.4-13 1.9-18.3 2.5 5.3 1.7 12.2-2.2 18.3-2.5zM260.8 253c4.9 3.7 5 12.2 9.2 16.6.3-8.4-4.5-9.1-6.7-19.1 2.8.3 3.3 2.8 4.2 5 1.9-4.7-5.5-7.2-9.2-9.2.4 1.2 1.2 2.1 1.7 3.3-3.7-.7-4.2-4.7-6.7-6.7 1.5 6.6 7.5 15.8 10.8 23.3-7.4-4.5-10.6-20.4-17.5-28.3 5.3 16.7 17 34 21.6 54.1 2.4-6-4.4-16.5-5.8-24.1 4.4 1.4 3.6 8 6.7 10.8 2.2-.7-1.6-3.7.8-5-3.3-.5-6.7-13.7-9.1-20.7zm-313 0c3.3.3 4.6-1.5 8.3-.8-.4 3.2-6.7.5-6.7 4.2 8.7-.7 22.3-6.4 29.1-8.3-.7.2 10.3 0 3.3-.8-2.5-1.1-3 .4-6.7 0 1.2-2.2 4.3-2.4 6.7-3.3-6.6-11.5-23.8-1.1-35 1.7-1 2.3.1 4.8 1 7.3zm80.8-1.7c-3.6-.2-1.1 4.7.8 4.2-.6 1.9-4.7.3-6.7.8 1.3 2.6 6.4 1.4 10 1.7-1.3-2.3-3.1-4.1-4.1-6.7zm-50 .9c-9-1.2-17.9 4.8-27.5 5.8 8.7.8 18.4-4.4 27.5-5.8zm-9.9 5.8c-2.6-2-7.3 1-10 1.7 1.9 1.3 6.6-1.6 10-1.7zm63.2 21.6c-3.2 2.7-6.8 4.9-11.7 5.8 1.8-7.5 11.4-5.2 16.6-7.5-3.5-7.6-16.3-18.6-24.1-9.2 4.5-2.4 8.7-1.4 12.5.8-5 2.8-11.5 4-16.6 6.7-.8-2.8 6.6-4.5 10-5.8-5-1.6-11.4 2.4-15.8 4.2-1.8-2.1 1.7-3.8 4.2-4.2-9.1 2.9-19.2 3-31.6 4.2.2 21.8 23.9 30.4 40.8 38.3 4.7 2.2 6.5 5.5 13.3 5.8-1.8-4.6-6.8-5.9-8.3-10.8 6.8 2.2 10.1 12 17.5 10.8-5.3-5.2-13.1-8-15.8-15.8 7.5 4.5 12.8 15.4 22.5 15-10.1-5.2-17.3-13.2-24.1-21.6 2.3-2 5.8 2 5.8 5 7.3 5.2 13.2 11.8 21.6 15.8-2.2-4.7-12.1-9-15.8-15 6.4-.7 11.2 11.4 18.3 10-4.5-3.2-9.1-6.5-12.5-10.8 3.9-1.7 5.2 5.7 10.8 5-.7-2.9-5.1-2.1-5-5.8.8-2.3 3.1.7 3.3 1.7 6-5.6 22.7-5 32.5-4.2 2.6-1.6-1-3.9-2.5-4.2-8 3.1-20.8 1.4-28.3 5-3.2-1.2-4-4.8-7.5-5.8 1.3-2.3 4.9 0 5 1.7 1.9-1.1-2.1-3.4-2.5-5 3-2.6 4.3 1.9 7.5 1.7 2.2-2.2-2.5-1.8-2.5-4.2 4.9 2.1 9.6 1 12.5-.8.4-3.4-2.2.8-2.5-2.5 3.6-.3 4.6 2 7.5 2.5 3.4.4 0-2.1 0-3.3 6.8 3.2 14.5 2 16.6 10.8 4.6-2.9-4.1-6.3-5-9.2 8.9.3 15.7 17.9 9.2 26.6 10.1-8.4.5-26.5-6.7-30.8-17.7-5.1-40.8-.7-51.6 10.8 2.8 2.5 5.7 4.9 8.3 7.5-3.4 1.7-7.9-2.6-9.2-5.8-2.5 1 .6 3.2.8 4.2-3.2-.1-3.3-3.4-5.8-4.2.3-2.8 2.5-3.6 4.2-5-2-2.7-2.5 3-5.8 1.7 1.1-7.4 9.2-2.7 9.9-10.1zm66.6-3.3c-1.2.3 1.1 5.7 2.5 9.2.9-1.6 2.1-3.9 2.5 0 4.3-3.1-4.8-5.2-5-9.2zm0 14.2c.2-2 2.3.9 2.5 1.7.5-3.3-2.6-10.2-6.7-11.7.9 5.5 2.2 10.6 5 14.1 1.1.4.1-3.8-.8-4.1zm8.3-5c1.7 2.8 1.8 7.1 4.2 9.2 1.9-1.7-2.3-4.3-.8-8.3-1.5 0-1.8-1.1-3.4-.9zm-4.1.8c-.3 2.3 4.7 7.4.8 11.7 2.7 2.9 5.4 5.7 7.5 9.2.5-2.7-1.3-10.3-5-11.7 2.6-4.6-2.5-5.7-3.3-9.2zm13.3 2.5h-2.5c2.2 3.9 2 10.2 6.7 11.7-1.3-4.1-3.4-7.2-4.2-11.7zm-46.6 0c-5.3 1.6-13.4.5-15 5.8 5.9-.3 13.9-1.6 18.3-.8-.3-3.2-6.2-.8-3.3-5zm257.2 5.8c-9.3 14.6-19.5 32.7-31.6 41.6-.6 2.3-3.9 6.1-1.7 8.3 11.7-9.1 19.3-22.3 28.3-34.1-3.9 12-12.6 21.8-21.6 31.6-4.5 4.9-9.8 7.8-10.8 15.8 14.4-9.2 22.1-25.1 33.3-37.5-.6-4.2 1.4-5.8.8-10 0-2.1-.6-1-1.7-.8 1.2-5.3 4.9-8.3 5-14.9zm-243.9 4.2c-9.4.2-13.4 2.2-25.8 1.7 5.8 3.2 21.4 2 25.8-1.7zm37.4 13.3c5.2.6 4.8 6.9 9.2 8.3 1-2.7-6.6-8.3-4.2-12.5-.4-2.9-4.6-2-5.8-4.2-2 1.3 1.8 3.7 1.7 5.8-2.8-1.1-5.2-4-3.3-7.5-2.6.4-1.9-2.6-5-1.7-.5 4.4 3.9 9.6 6.7 13.3.3 1.4-.6 1.6-1.7 1.7 1.9 3.3 5.7 4.8 9.2 6.7-.4-1.9-5.2-6.4-6.8-9.9zm-66.6-6.7c9.2 2.9 19.8-.7 29.1-.8-4.7-2.8-20.4-.2-29.1.8zm52.5-1.6c-1 3.3 6.8 4.9 6.7 10 1.3.1 3.2-.4 3.3.8.9-4.5-8.3-6.1-10-10.8zm-22.5 5c-4.8-3.7-19.9.7-28.3 0 2.9 3.7 9.9 3.4 16.6 3.3-1.4-2-5.9-.7-7.5-2.5 5.9.2 11.6-.9 19.2-.8zm49.1-.9c-.9 8.5 7.2 15 12.5 20-1.2-3-8.1-14.6-12.5-20zm-88.2 50c-.3-1.7.2-2.6 1.7-2.5.2-2.4-1.9-2.6-1.7-5 4.2-3.6 3.4 4.9 6.7 5-.9-2.2-.5-5.6-2.5-6.7.3-1.1 2.1-.7 3.3-.8-3-4.7-5.7-9.8-7.5-15.8 5.7 1.5 2.3 8.5 7.5 9.2-.1-4.5-1.9-7.5-3.3-12.5 7 2.7 8.8 12.3 10.8 17.5-.2.9-1.7.6-1.7 1.7-2.9-2.6.2-6.7-5-7.5 3.1 5.3 3.9 12.8 7.5 17.5-4.2-.5-3.3-6.1-7.5-6.7.5 3.4 3.6 4.2 3.3 8.3-2.9 1.2-2.6-5-4.2-2.5-3.5-.1 0 1.3 0 2.5-1.9 1.8-2.9-.4-5.8.8 15.1 5.1 29.7-7.1 43.3-4.2-2.8-2.5-11.7 1.1-13.3-2.5-1 .1-.7 1.5-.8 2.5-5.9-.8-6.2-7.1-10-10 .6-2.6 3.9-3.4 3.3.8 2.4-.7-.5-2.7 0-4.2-6 1.3-6.5-6.5-5.8-9.2 2.2 1.2 2.4 4.3 3.3 6.7 2.8-3.7-3.6-8.8-2.5-10 2.2-2.4 5.9 4.7 5.8 10.8 5.6-2.7-2.1-7.4-1.7-12.5-8.1-3.7-21.8-5.6-32.5-1.7-1-.4-1.7-1.1-1.7-2.5-16.2.9-27-7.9-40.8-16.6-3.3 1.7-2 8-6.7 8.3 4.3 6.4 5.5 14.7 10 22.5 1.2-.5 2-1.3 2.5-2.5 2.8.5 3.9 2.7 3.3 6.7 4.1-5.6 4.3 6 6.7.8 2.3 1.6-1 8.7 3.3 8.3-1.1-2.6.2-2.4 0-5.8 3.6.1 1.9 5.3 3.3 7.5 1.8-.7.7-4.3 2.5-5 1.3 2.3 1.4 5.8 2.5 8.3 2.3-.8-.8-6.9 2.5-6.7 2.7 1.2 1.3 6.4 3.3 8.3 2.4.1-.7-5.1 1.7-5 3 0 1.2 4.9 5 4.2-.7-1-1.1-2.2-.8-4.2 3.1-.1 2.8 3.3 3.3 5.8 3.4.6-.9-2.9 1.7-4.2 1.9.6 2.5 2.5 3.3 4.2 2.1-.1.2-2.8 0-3.3 2.6-.4 3.4 1 4.2 2.5 1.1.1.7-.6.2-.6zm82.4-49.1c2.3 7.1 6.1 12.8 10.8 17.5-2-4.2-5.1-14.3-10.8-17.5zM56 314.6c5 2.7 17.3 2.9 24.1.8-6.4-.7-17.2-.3-24.1-.8zm45.8 3.3c3.8 2.3 6.9 5.3 10.8 7.5-2.9-3.2-4.4-7.8-10.8-7.5zm-18.3 0c-1.9.2-.5 3.7 0 4.2 5.9-1.1 10.9-1.3 15 .8-4.1 3.3-11.6-2.1-12.5 4.2 4.1-2 11.9-.3 15-3.3 2.3.5 3.9 1.7 5 3.3 2.6-3.7-5.8-3.6-5.8-7.5-7.4.5-13.9 2.2-16.7-1.7zm-2.5 3.4c-1.8-.7-6.8-.9-10 .8 1.6 2.3 5.5-.4 10-.8zm164 .8c.3 8.1 8 15.9 13.3 21.6-3.4-8.3-9.4-13.8-13.3-21.6zm-3.3 2.5c1.6 8.9 10.2 22.5 17.5 25.8 2.4-2.3-1-2.5-2.5-3.3-5.1-7.5-9.9-15.2-15-22.5zm40.7 59.1c13.5-14.6 30.5-25.6 38.3-45.8-7.7 6.1-12.3 14.7-19.1 21.6-7 7.1-17.1 12.5-19.2 24.2zm39.2-56.6c-12 11.6-21 26.2-34.1 36.6 1.1 4.5-.2 4.9-2.5 7.5 5.1.1 7.3-9.9 14.1-10.8 6.4-12.2 18.7-18.5 22.5-33.3zm-78.3 15.8c5.8 2 6.5 9.1 10.8 12.5-1.5-9.9-11.6-18.4-15.8-28.3-4.5 5.2 5.8 8.9 5 15.8-4.4-1.4-5.3-6.4-8.3-9.2-.1 10.3 10.2 17.5 15.8 25 1.4-5.4-8.8-10.4-7.5-15.8zm-168.1-5.8c9.7-1.7 19-3.8 28.3-5.8-2.3-2.4-9.5.5-10.8-.8.8-1.4 3.5-.9 5-1.7-6.6-2.8-21.5-.1-22.5 8.3zm-5-10c-2.1 1.3 1.5 4 1.7 5.8 3.7-1.3-.5-4.4-1.7-5.8zm45.8 16.6c-.8-5.8 2.4-9.9-1.7-15-1.1 3.4 1.7 14-1.7 17.5 2.2.3.7-3.2 3.4-2.5zm44.9-10.8c2.4 7 11 15 20 18.3-6.9-5.9-13.4-12.1-20-18.3zm-84 5.8c-2.6.2 1.3 6.9-2.5 5.8 3.3.9 3.3 5 5 7.5 2.1-.7 4.5-1.1 7.5-.8 1.2.3.1 1.5-.8 1.7 5.6 2 13-2.9 17.5-2.5-2.2-3.2-9.9.7-14.1.8.3-2.7 4.1-2 6.7-2.5-4.9-3.1-12 2.7-16.6 0 6.9-3.1 17.1-2.9 24.1-5.8-6.6-2.3-14.7 3.5-24.1 3.3 7.1-4.2 17.1-5.6 27.5-6.7-5-4.6-14.4 3.4-20.8 0 6.4-1.7 13-3.1 20-4.2-8.6-3.5-21.9 3.9-29.4 3.4zm153.1-2.5c.1 10.1 11.4 20.9 19.1 27.5.5.4 1.3 5.3 2.5 1.7-8.3-8.6-13.9-19.9-21.6-29.2zm-68.2 8.4c5.1 4.6 9.7 9.7 13.3 15.8-3.4 3.1-5-2.7-5.8-5-6.2-5.4-10-13.3-19.1-15.8 6.6 6.1 13.6 12 18.3 20-5.3 1.7-6.4-6.1-9.2-9.2-3.8-4.1-9.6-6-11.7-10.8-2.5.6-3-.9-5-.8 6.5 6.8 15.5 11.1 19.1 20.8-8.9-7.2-14.9-17.3-26.6-21.6 7 7.1 15.7 12.6 21.6 20.8-4.4 2.7-6.3-4.2-8.3-6.7-7.9-5.1-13.3-12.7-23.3-15.8 6.1 5.8 14.6 9.2 19.1 16.6-7.5-3.3-12.2-9.5-20-12.5.3 4.4 10.3 6.3 12.5 11.7-4.6.9-7.8-6.6-13.3-7.5 2 2.7 6.2 3.2 7.5 6.7-4.8 2.5-6.2-5.4-11.7-5 1.5 2.7 5.7 2.6 6.7 5.8-2.6 0-5.4-2-7.5 0 4.1 1.4 7.8 3.3 9.2 7.5-5.2 1-4.3-4-8.3-4.2.3 2.8 3.4 2.7 3.3 5.8-2.2 2.2-2.5-1-5-.8.8 2.8-1.2 2.7-3.3 2.5 2.3 3.3 2 2.4.8 6.7 3 3.7 4.2 9.1 5 15-5.2 0-1.8-8.7-7.5-8.3 1.8 15.7 9.6 31 23.3 37.5-5.2-7.3-12.9-11.5-13.3-20.8-2.8-1.1-4.1-3.7-5-6.7h3.3c5.2 13.4 13.9 23.3 25 30.8-4.4-6.5-15.1-13.7-18.3-24.1 8.8 6.5 13.7 16.9 23.3 22.5-2.9-6.8-14.5-12.2-16.6-22.5 8.7 7.4 13.9 18.3 25 23.3-6.9-6.7-14.1-13.1-19.1-21.6 7 .6 10.3 9.9 15.8 15 3.5 3.1 7.7 4.4 10.8 7.5-3.6-7-15.4-12.9-19.1-22.5 9.2 5.8 15.1 14.8 25 20-2.8-6.1-14-10.9-17.5-19.1 8.3-.8 10.9 11.3 20 12.5-1.6-5.4-8.5-5.3-10.8-10 8.5.2 14.2 10.2 23.3 12.5 1.3-3.2-7.1-3.9-9.2-6.7 6.9-1.6 12.7 5 20 5.8-9.1-5.3-18.7-10.1-25.8-17.5.2-2.9 3.1 2.1 3.3-.8 7.4 7 16.2 12.7 26.6 16.6-2.6-3.8-8.5-4.3-10.8-8.3 8.2.4 10.5 6.7 18.3 7.5-6.6-5.9-15.1-9.8-21.6-15.8 8.6.3 14.3 10.7 25 11.7-5.7-5.4-17.8-11.6-24.1-19.1 4.3.2 9.2 6.9 14.1 9.2 2.6-1.6-.5-4.4-1.7-5 7.4.1 13.3 8.9 21.6 10.8-6.1-6.3-13.4-11.5-20-17.5 7.2 2.6 15.3 11.3 24.1 15-11.9-10-24.3-26.8-39.1-36.6-.4-1.6-1.8-2.1-1.7-4.2-2.5.6-2.4-1.4-5-.8 6.3 5.3 11.9 11.4 15.8 19.1-9.6-6-13.9-17.1-23.3-23.3.2 4.3 7.5 8.5 9.2 14.1-9-5.2-11.1-16.9-21.6-20.2zm88.2 24.1c-9.3-9.3-17.3-19.9-25-30.8 2 10 14.4 23.9 25 30.8zm-77.4-31.6c1.2 6 8.7 12.9 16.6 15-4.8-5.8-11.5-9.6-16.6-15zm67.4 27.4c-4-5.2-11.6-13.9-17.5-20 1.7 3.3 3.4 6.6 5.8 9.2-4.8.1-9.6-7-15-9.2.6 3.6 4.5 3.8 5.8 6.7-6.1 1.1-9.7-7.5-14.1-10.8.8 7.8 12.4 12 17.5 18.3-9.5-4.7-20.8 2.8-26.6.8 2.8 8.8 15.7 14.8 20.8 24.1 5.8 2.3 8.5 7.6 15.8 8.3-2.1-3.4-6.9-4.2-8.3-8.3 5.2 1.1 7.6 5.1 11.7 7.5 1-2.7-5.1-5.5-6.7-8.3 4.6-.2 4.4 4.5 9.2 4.2-3.1-4.4-4.2-6.2-7.5-10.8 4.7-1 4.8 6.8 10 6.7-1.2-4.1-4.7-5.8-5.8-10 4.8.8 4.5 6.6 10 6.7-2.8-6.1-9.5-8.3-12.5-14.1 4.5 0 8.9 7.2 13.3 10-3.9-12.5-18.6-14.1-26.6-22.5 8.6-.5 13.8 6.3 20.7 11.5zm-49.9-24.1c3.3 7.8 9.9 12.3 17.5 15.8-2.8-5.1-12.1-10.7-17.5-15.8zm-7.5 0c1.3 5.9 9.8 11.8 16.6 15-4.3-6.3-10.8-10.3-16.6-15zm14.9 1.7c4.1 5 11.1 11.7 15.8 13.3-3.7-6-9.4-9.9-15.8-13.3zm-123.1 7.5h-2.5c.4 2.9 1.8 3.5 4.2 2.5-.6-.9-2-.9-1.7-2.5zm23.3 9.1c2.5 4.1 8.3 2.9 10 9.2 1 3.6-1.1 7.3-2.5 11.7 8.2-6.3 4.5-21.7-7.5-20.9zm-3.4 3.3c-6.5-2.7-17.5 1.7-25 3.3.4 2.1 1.6 3.4 2.5 5 3.7.1.2-2.4 0-3.3 6-3.1 13.7-4.7 22.5-5zm3.4 22.5c-5.5-.5-12.7 4.5-20.8 4.2.4.7.9 1.4.8 2.5-7.6-5.4-10.2-15.9-15-24.1-8.6-1.6-14.9 1.7-22.5.8-11.6-1.3-20.4-9.8-32.5-6.7-.1 1.3.4 3.2-.8 3.3.2 1.7 2.2-1.2 3.3.8-.9 10.5 5.8 17.9 10.8 25 1.9-1.1-1.8-3.7-1.7-5.8 3.9 1.9 5.4 6.3 8.3 9.2 1.1-1.2-1.3-5.9-2.5-7.5 5 1.1 5.3 6.9 9.2 9.2-2.4 2.5-4.6.7-9.2 1.7 4 2.6 9.2-.7 14.1 0-3-1.4-6.2-2.7-6.7-6.7 1.6-.2 6.2 3.8 7.5 6.7 13.5-4.1 31 0 45.8-5.8-.1-1-1.5-.7-2.5-.8 5.4-3 10.4-1.4 14.4-6zm-10.9-18.3c-5.5.3-4.6 2-10 2.5-.8 2.5 1.9 1.5 3.3 1.7-.7 1.8-3.7 1.3-5 2.5.1 3.1.9 2.4 0 5 9.2-.6 15.2-4.2 23.3-5.8-3.6-2.5-8.1 1.3-12.5.8 1.4-3.9 9.2-1.3 10.8-5-6.3-1.1-8 2.5-14.1 1.7.3-2.4 4.8-.4 4.2-3.4zm-19.9 0c-3.3 1.5 1.1 7.1 3.3 8.3 1.2-2.2-3.8-4.1-3.3-8.3zm-60 4.2c-1.3 7.3 7.9 18.1 14.1 23.3-4.9-7.6-11.3-13.6-14.1-23.3zm96.6 4.1c-8.8-1.1-19.4 3.4-26.6 6.7-.7 2.4 1.2 2.2.8 4.2 13.9 1.6 13.6-10.8 25.8-10.9zm3.3 9.2c-.1-3.5-6.7-1.1-8.3-.8 1.1 1.3 6-1.2 8.3.8zm2.5 0v5.8c-3-.2-3.2-1.1-5.8 0 1.2 4.1 3 7.6 4.2 11.7-4.9-.1-4.8-5.2-6.7-8.3-3 1.3.3 3.2-5 1.7.9 8.5 5 13.9 10.8 17.5-2.6-4.1-5.4-7.9-6.7-13.3 6 3.8 10.9 15.7 20 19.1-3.7-4.6-8.5-8.2-10.8-14.1 6.6 3.1 8.7 10.7 16.6 12.5-7.2-6.7-17.9-14.3-17.5-25.8 3.5 2.9 4.5 8.2 6.7 12.5 4.6 2.1 6.4 6.9 11.7 8.3 1.8 3.4 1.7-.3-.8-.8-4.2-4.7-10.5-11.6-10.8-17.5-.5-1.2-1.3-2-2.5-2.5-1-4.3 1.1-6.4-3.4-6.8zm95.7 9.1c3.2 5.1 11 5.6 15.8 9.2-2-2.6-10.1-10.1-15.8-9.2zm-110.7 3.4c-7.8 4.1 5.6 19.3 11.7 22.5-3.9-4.2-10-13.3-11.7-22.5zm-6.6 2.5c-1.5 7.6 3.8 15.6 10 18.3-3.8-5.7-7.8-11.1-10-18.3zm223 4.1c-3.7-2-5.3 5.2-9.2 5.8 4.2 2.5 6-4.6 9.2-5.8zm-228-.8c.1 7.4 4.8 10.2 7.5 15-1.2-3.1-3.1-12.5-7.5-15zm-25 2.5c-.8 3 4.9 11.2 7.5 10.8-2.7-3.4-4.9-7.3-7.5-10.8zm23.3 1.7c-4.2 3.5-5.1 12.8 1.7 13.3-1-3.4-4.2-4.7-3.3-10 3.8.7 2.4 6.4 6.7 6.7-1.6-3.6-4.2-6-5.1-10zm-12.5 0c-2 2 2.6 4.6 3.3 6.7 1.6-1.7-3.2-4-3.3-6.7zm-5.8 0c1.8 1.8 1 6.2 5 5.8-.3-3.3-2.9-4.3-5-5.8zm-9.2 0c-3.4 3.6 4 8.1 7.5 10-1.8-4-5.4-6.3-7.5-10zm-7.4 0c-1.5 2.9 4.5 5.5 5.8 8.3.9-2.3-4.8-5.2-5.8-8.3zm129.8 1.6c1.1 3.1 4.4 4 7.5 5-2-2.1-4.6-3.7-7.5-5zm66.6 12.5c1.8 3.2 5.1 4.9 10 5-3.1-2-6.2-3.8-10-5z" />
                                <path d="M126.8 199.7c-1-2.7-5.8-1.4-5.8-5 1.8 1.3 7.8 2.7 5.8 5zm15.8 102.4c-7.5-5-10.1-19.3-11.7-27.5 4.2 9 7.9 18.3 11.7 27.5zm54.9 14.2c2.6 3.9 6.3 8.2 9.2 11.7 2.5 3 6.6 5.4 5.8 8.3-13.6-9.5-18.4-22.7-26.6-37.5 6.3 3 7.6 11.4 11.6 17.5zm129.9 33.3c.3-3.5.3-7.5 3.3-8.3h-2.5c1.6-13.5 4.7-21.4 7.5-33.3-.8 9.6.1 22-5.8 30.8 2.6 1.4 1.4 10.5-2.5 10.8zm-258 7.4c-3.1-2.2-4.5-6-6.7-9.2 3.7 1.7 5.3 5.3 6.7 9.2zm102.3 30.8c-5.5-.1-9.3-6.3-9.2-9.2 2.9 3.3 7 5.4 9.2 9.2zm8.4 13.4c-8.7-1.2-13.5-6.5-17.5-12.5 2.9-1.9 10.9 4.1 6.7 6.7 5.5-.1 7.8 3.1 10.8 5.8z" />
                            </svg>
                        </button>
                    </div>
                    <!--  /row  -->
                    <div class="row">
                        <div class="col-md-12 fine">
                            <p>This registration form is not an offering for sale. An offering for sale may only be made by way of Disclosure Statement. By completing this registration form you are giving us consent to send you information and to contact you about this and possible future interest.
                                <br />
                                <br />We do not sell or give personal information to third parties.</p>
                        </div>
                    </div>
                    <!-- .row -->
                </fieldset>
            </form>
            <!-- /Form  -->
        </div>
        	<!-- /container-fluid  -->
</div><!-- register content -->

<?php include('footer.php'); ?>