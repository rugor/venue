<div class="container-fluid">
	
	<div id="floorplan_content" class="swiper-container">
	
		<div id="plan_type_wrap" class="swiper-wrapper">
		
		<?php foreach($planTypes as $plans) { ?>
			
			<div class="the_types swiper-slide plan_<?php echo $plans; ?>" data-hash="<?php echo $plans; ?>">
				
				<div class="slide-contain">
				
				<?php 
					foreach($planArr as $theplan) { 
						if ( $theplan['series'] == $plans ) {
				?>

					<div class="row">
						<div class="col-sm-4">
								
							<img class-"plan_logo" src="<?php echo $theplan['logo']; ?>" alt=""/>
							
							<div class="plan_info">
							
								<div class="plan_info_l">
									<p><?php echo $theplan['type']; ?></p>
									
									<?php
									// http://www.advancedcustomfields.com/resources/select/

									/* $field = get_field_object('field_name');
									$value = get_field('field_name');
									$label = $theplan['choices'][ $value ]; */
									?>
								</div><!-- plan info l -->
								
								<div class="plan_info_r">
									<p><?php echo $theplan['ranger']; ?> sq. ft.</p>
								</div><!-- plan info r -->
								
								<a href="<?php echo $theplan['PDF']; ?>"  target="_blank" class="info_pdf">View PDF</a>
							
							</div><!-- plan info -->
						
						</div>

						<div class="col-sm-8">

							<img src="<?php echo $theplan['image']; ?>" alt=""/>

						</div>
					
					</div><!-- /row -->
					
				</div><!-- /slide-contain -->
			<?php 
					} 
				}
			?>
			</div><!-- the types -->
		<?php } ?>
		</div><!-- /plan type wrap /swiper-wrapper-->

		<!-- Add Pagination -->
	    <div class="swiper-pagination"></div>
	    <!-- Add Arrows -->
	    <div class="swiper-button-next"></div>
	    <div class="swiper-button-prev"></div>

		<div style="clear: both;"></div>
	</div><!-- floorplan content -->
</div><!-- /container-fluid -->