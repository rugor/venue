<?php
register_nav_menu( 'primary', 'Main Menu' );

/**
* 	Works in WordPress 4.1 or later.
*/
if ( version_compare( $GLOBALS['wp_version'], '4.1-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
}

add_filter('upload_mimes', 'custom_upload_mimes');

function custom_upload_mimes ( $existing_mimes=array() ) {
    $existing_mimes['svg'] = 'mime/type';
    return $existing_mimes;
}
	
/**
 * Proper way to enqueue scripts and styles
 */
function tenoaks_scripts() {
	wp_enqueue_style( 'style-reset', get_template_directory_uri() . '/css/reset.css' );
	wp_enqueue_style( 'style-lightbox', get_template_directory_uri() . '/css/lightbox.css' );
	wp_enqueue_style( 'style-main', get_stylesheet_uri() );
	wp_enqueue_script( 'plugin-script', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0.0', true );
	wp_enqueue_script( 'main-script', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'tenoaks_scripts' );

// #####
// Floorplan Custom Post Type
// #####

// Add new post type for Projects
add_action('init', 'marca_fp_manager_init');
function marca_fp_manager_init() 
{
    $floorplan_labels = array(
        'name' => _x('Floorplans', 'post type general name'),
        'singular_name' => _x('Floorplan', 'post type singular name'),
        'all_items' => __('All Floorplans'),
        'add_new' => _x('Add new floorplan', 'floorplans'),
        'add_new_item' => __('Add new floorplan'),
        'edit_item' => __('Edit floorplan'),
        'new_item' => __('New floorplan'),
        'view_item' => __('View floorplan'),
        'search_items' => __('Search in floorplans'),
        'not_found' =>  __('No floorplans found'),
        'not_found_in_trash' => __('No floorplans found in trash'), 
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $floorplan_labels,
        'public' => true,
        'menu_icon' => 'dashicons-hammer',
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 15,
        'supports' => array('title'),
        'has_archive' => 'floorplans'
    ); 
    register_post_type('marca_fp_manager',$args);
    // register_taxonomy_for_object_type('post_tag', 'marca_fp_manager');
    // register_taxonomy_for_object_type('category', 'works');
}

// list cpt categories

// Include Custom Post Types from posttypes.php

//include_once(ABSPATH . 'wp-content/themes/teanoaks/posttypes.php');

// #####
// Shortcodes
// #####

function heading_func( $atts, $content ){
	return '<h1>' . $content . '</h1>';
}
add_shortcode( 'heading', 'heading_func' );

function sub_heading_func( $atts, $content ){
	return '<h2>' . $content . '</h2>';
}
add_shortcode( 'sub_heading', 'sub_heading_func' );

function text_func( $atts, $content ){
	return '<p>' . $content . '</p>';
}
add_shortcode( 'text', 'text_func' );

// remove junk from head

function disable_embeds_init() {

    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action( 'wp_head', 'rsd_link');
    remove_action('wp_head', 'wp_generator');

    // Remove the REST API endpoint.
    // remove_action('rest_api_init', 'wp_oembed_register_route');

    // Turn off oEmbed auto discovery.
    // Don't filter oEmbed results.
    remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);

    // Remove oEmbed discovery links.
    remove_action('wp_head', 'wp_oembed_add_discovery_links');

    // Remove oEmbed-specific JavaScript from the front-end and back-end.
    remove_action('wp_head', 'wp_oembed_add_host_js');
}

add_action('init', 'disable_embeds_init', 9999);
?>