(function($) {
	
	$(document).ready(function() {

		$('#corpmenu li').hover(function() {
			
			if ( $(this).children('.sub-menu').length > 0 ) {
				$(this).children('.sub-menu').fadeIn(300);
			}
			
		}, function() {
			
			$(this).children('.sub-menu').fadeOut(300);
			
		});
		
		$('#submitbtn').click(function() {
			
			var empties = [];
			
			$('.required').each(function() {
				var $this = $(this);
				
				if ( $this.val() == "" ) {
					$this.css({ background : "#f38383" });
					empties.push('not empty');
				}
				
			});
			
			if ( empties.length > 0 ) {
				return false;
			}
			
		});
		
		$('#hamburger').click(function() {
			
			if ( $(this).hasClass('opened') == false ) {
				$(this).addClass('opened');
				$(this).find('ul').show();
			} else {
				$(this).removeClass('opened');
				$(this).find('ul').hide();
			}
			
		});
		
		$('#more_text_home').hide();
		
		$('.open_more_text').click(function(event) {
			
			if ( $(this).hasClass('clicked') == false ) {
				$(this).addClass('clicked');
				$(this).html("LESS -");
				$('#more_text_home').show();
			} else {
				$(this).removeClass('clicked');
				$(this).html("MORE +");
				$('#more_text_home').hide();
			}
			
			event.preventDefault();
		});
		
		$('#gallery_grid').masonry({
			 columnWidth: '.grid-sizer',
			 itemSelector: '.grid-item',
			 percentPosition: true
		});
		
		// $(".fancybox").fancybox();
		$('.fancybox').fancybox();
		
		$(".fancybox_video").fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			autoSize	: true,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
		
		/* Floorplans */
		
		$('.hover_box:before').hover(function() {
			
			$(this).parent().addClass('hovered');
			
		});
		
		$('.units').hover(function() {
			
			var unit = $(this).attr('id');
			var img = unit.replace("unit_", "");
			var series = $(this).data("series");
			var total = $(this).data("totalsqft");
			var offset = $(this).position();
			
			$('#siteplan_wrap').prepend('<div class="unit_info"><p><span>' + series + '</span><br/>' + total + ' sq. ft.</p></div>');
			$('.unit_info').css({
				top : ( offset.top - 85 ) + 'px',
				left : ( offset.left - 140 ) + 'px'
			});
			$('#siteplan_wrap').append('<img class="unit_hl" src="' + THEMEURL + '/images/hovers/' + img + '.png" alt=""/>');
			
		}, function() {
			
			$('.unit_hl').remove();
			$('.unit_info').remove();
			
		});
		
		$('.units').click(function(event) {
			
			var unit = $(this).attr('id');
			var img = unit.replace("unit_", "");
			var series = $(this).data("series");
			var total = $(this).data("totalsqft");
			var offset = $(this).position();
			var wOffset = $('#' + series + '_wrap').offset();
			
			$('html, body').animate({
				scrollTop : ( wOffset.top ) + 'px'
			}, 500);
			
			event.preventDefault();
		});
		
		/* Neighbourhood */
		
		$('.map_cats h3').click(function() {
			
			var id = $(this).attr('id');
			var target = id.replace("_title", "");
			$('.map_cats h3').removeClass('active');
			$(this).addClass('active');
			
			$('.map_locs').hide();
			$('#' + target).fadeIn(300);
			
		});
		
	});
	
	$(window).scroll(function() {
		
		var theTop = $(window).scrollTop();
		
		if ( theTop >= 50 ) {
			$('#header').addClass('popped');
		} else {
			$('#header').removeClass('popped');
		}
		
	});
	
	var vid = document.getElementById("bgvid");
	
	if ( vid ) {
		vid.onloadedmetadata = function() {
		    $('.sk-circle-wrap').fadeOut();
		};
	}
	
})(jQuery);