<?php include('header.php'); 
/* Template Name: Thank You Template */
$content = get_field('content');
?>

<?php if ( $logo_small_uploader ) { ?>
	<a href="<?php bloginfo('url'); ?>"><img src="<?php echo $logo_small_uploader; ?>" id="small_logo" alt="Ten Oaks"/></a>
<?php } ?>

<div id="thankyou_content">
	<img src="<?php bloginfo('template_directory'); ?>/images/thankyou.png" id="thankyou" alt="thanks"/>
	<?php if ( $content ) { ?>
		<?php echo do_shortcode($content); ?>
	<?php } ?>
	<img src="<?php bloginfo('template_directory'); ?>/images/phrase.png" id="emailgift" alt="it's our way of saying thanks"/>
</div><!-- register content -->

<?php include('footer.php'); ?>